#https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3237/
import unittest as test

class Solution:
    def findNumbers(self, nums) ->int :
        countOfOddNumbers = 0
        for num in nums:
            rank = 1
            while num / 10 >= 1:
                num = num / 10
                rank = rank + 1
            if rank % 2 == 0:
                countOfOddNumbers = countOfOddNumbers + 1
        return countOfOddNumbers


class TestSolution(test.TestCase):
    def test_findNumbers(self):
        self.cases = ({'list':[12,345,2,6,7896], 'expected':2}, {'list':[1,12,12,12], 'expected':3},
                      {'list':[], 'expected':0},
                      {'list':[100000], 'expected':1},)
        for x in self.cases:
            with(self.subTest(case=x)):
                self.assertEquals(x['expected'], Solution.findNumbers(self, x['list']))
