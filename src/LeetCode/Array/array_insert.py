import unittest as test
class Solution:
    def duplicateZeros(self, arr):
        indexLag = 1
        copiedList = arr[:]
        for index, value in enumerate(copiedList):
            if value == 0:
                self.insertElement(arr, index+indexLag, 0)
                indexLag = indexLag +indexLag

    def insertElement(self, sortedList, i, el):
        sortedList.insert(i, el)
        sortedList.pop(len(sortedList)-1)


class TestSolution(test.TestCase):
    def test_duplicateZeros(self):
        self.cases = (
                      {'expected':[], 'list':[]},
                      {'expected':[1,0], 'list':[1,0]},
                      {'list':[1,0,2,3,0,4,5,0], 'expected':[1,0,0,2,3,0,0,4]}
        )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.duplicateZeros(x['list'])
                self.assertEquals(x['expected'],x['list'])
    def test_insertElement(self):
        self.cases = (
            {'list':[1,0], 'expected':[1,0], 'index':2, 'element':0},
        )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.insertElement(x['list'], x['index'], x['element'])
                self.assertEquals(x['expected'], x['list'])

