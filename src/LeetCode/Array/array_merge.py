import unittest as test

class Solution:
    def merge(self, nums1, m, nums2, n):
        nums2Index = 0
        for index, value in enumerate(nums1):
            if((nums2Index < len(nums2)) and n > nums2Index and nums2[nums2Index] <= value):
                nums1.insert(index, nums2[nums2Index])
                nums1.pop()
                nums2Index = nums2Index + 1

        while(n >= (nums2Index +1)):
            index = m + nums2Index
            nums1.insert(index, nums2[nums2Index])
            nums1.pop()
            nums2Index +=1





class TestSolution(test.TestCase):
    def test_merge(self):
        self.cases=({'nums1':[1,2,3,0,0,0], 'm':3, 'nums2':[2,5,6], 'n':3, 'expected':[1,2,2,3,5,6]},
                    {'nums1':[1], 'm':1, 'nums2':[], 'n':0, 'expected':[1]})

        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.merge(x['nums1'], x['m'], x['nums2'], x['n'])
                self.assertEquals(x['expected'], x['nums1'])
