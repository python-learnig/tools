# https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3247/
import unittest as test
class Solution:
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        count = 0
        for i in range(len(nums)):
            if nums[i] != val :
                nums[count] = nums[i]
                count +=1
        return count

class TestSolution(test.TestCase):
    def test_removeElement(self):
        self.cases = (
            {'nums':[3,2,2,3], 'val':3, 'expected':[2,2]},
            {'nums':[0,1,2,2,3,0,4,2], 'val':2, 'expected':[0,1,3,0,4]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.removeElement(x['nums'], x['val'])
                self.assertEquals(x['expected'], x['nums'])
