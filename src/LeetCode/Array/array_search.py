# https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3238/
import unittest as test

class Solution:
    def findMaxConsecutiveOnes(self, nums) -> int:
        count = 0
        maxSequence = 0
        for i in nums:
            if i == 1:
                count+=1
                if maxSequence < count:
                    maxSequence = count
            else:
                count = 0
        return maxSequence






class TestSolution(test.TestCase):

    def test_findMaxConsecutiveOnes(self):
        self.cases = ({'list':[1,1,0,1,1,1], 'expect':3},
                      {'list':[1,1], 'expect':2})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(Solution.findMaxConsecutiveOnes(self, x['list']), x['expect'])
