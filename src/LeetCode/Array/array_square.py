# https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3240/
import unittest as test

class Solution:
    def sortedSquares(self, list):
        sortedList = []
        for i in list:
            sortedList = self.setToList(sortedList, i*i)
        return sortedList

    def setToList(self, sortedList, el):
        newSortedList = sortedList[:]
        for i, value in enumerate(sortedList):
            if value < el:
                continue
            newSortedList=self.insertElement(sortedList, i, el)
            return newSortedList

        newSortedList.append(el)
        return newSortedList

    def insertElement(self, sortedList, i, el):
           beforeElement = sortedList[:i]
           afterElement = sortedList[i:]
           return beforeElement + [el] + afterElement

class TestSolution(test.TestCase):
    def test_sortedSquares(self):
        self.cases = (
                      {'list':[0,1], 'expected':[0,1]},
                      {'list':[-1,0], 'expected':[0,1]},
                      {'list':[-4,-1,0,3,10], 'expected':[0,1,9,16,100]},
                      {'list':[-7,-3,2,3,11], 'expected':[4,9,9,49,121]})
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                self.assertEquals(x['expected'],solution.sortedSquares(x['list']))
