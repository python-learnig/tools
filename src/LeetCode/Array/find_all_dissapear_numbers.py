#https://leetcode.com/explore/learn/card/fun-with-arrays/523/conclusion/3270/

import unittest as test

class Solution(object):
    def findDisappearedNumbers(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        # For each number i in nums,
        # we mark the number that i points as negative.
        # Then we filter the list, get all the indexes
        # who points to a positive number
        for i in range(len(nums)):
            index = abs(nums[i]) - 1
            nums[index] = - abs(nums[index])

        return [i + 1 for i in range(len(nums)) if nums[i] > 0]




class TestSolution(test.TestCase):
    def test_findDisappearedNumbers(self):
        self.cases = (
            {'arr':[1,1], 'expected':[2]},
            {'arr':[4,3,2,7,8,2,3,1], 'expected':[5, 6]},
            {'arr':[4,3,2,7,7,2,3,1], 'expected':[5, 6, 8]},

        )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                self.assertEquals(x['expected'], solution.findDisappearedNumbers(x['arr']))

