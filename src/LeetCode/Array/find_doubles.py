# https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3250/
import unittest as unit

class Solution(object):
    def checkIfExist(self, arr):
       for i, value in enumerate(arr):
            doubleValue = value * 2
            for index, v in enumerate(arr):
                if index == i:
                    continue
                if v == doubleValue:
                    return True
       return False



class TestSolution(unit.TestCase):
    def test_findSolution(self):
        self.cases = (
            {'array': [10, 2, 5, 3], 'expected': True},
            {'array':[2,5,3], 'expected': False},
            {'array':[-2,0,10,-19,4,6,-8], 'expected': False},
                      )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                self.assertEquals(x['expected'], solution.checkIfExist(x['array']))

