import unittest

class Solution(object):
    def heightChecker(self, heights):
        copyHeights = heights[:]
        heights.sort()
        counter = 0
        for index, value in enumerate(heights):
            if(copyHeights[index] != value):
                counter = counter + 1
        return counter
            
class TestSolution(unittest.TestCase):
    def test_heightChecker(self):
        self.cases = ({'heights':[1,1,4,2,1,3], 'expected':3},)
        for x in self.cases:
            with self.subTest(case = x ):
                solution = Solution()
                self.assertEquals(x['expected'], solution.heightChecker(x['heights']))
