import unittest as test

class Solution(object):
    def validMountainArray(self, arr):
        states=['up','down']
        state = 'not-select'
        for i in range(len(arr)-1):
            if(arr[i] == arr[i+1]):
                return False
            if(state == 'not-select' and arr[i] > arr[i+1]):
                return False
            if(state == 'not-select' and arr[i]<arr[i+1]):
                state = states.pop(0)
                continue

            if(state == 'up' and arr[i]> arr[i+1] ):
                if(len(states) == 0):
                    return False
                state = states.pop(0)
                continue
            if(state == 'down' and arr[i] < arr[i+1]):
                return False
            if(state == 'up' and arr[i] < arr[i+1]):
                continue
        if(len(states)):
            return False
        return True



class TestSolution(test.TestCase):
    def test_validMountainArray(self):
        self.cases = (
            {'array':[2,1], 'expected':False},
            {'array':[0,3,2,1], 'expected':True},
            {'array':[0,1,2,4,2,1], 'expected':True},
            {'array':[2,1,2,3,5,7,9,10,12,14,15,16,18,14,13], 'expected':False},
            {'array':[14,82,89,84,79,70,70,68,67,66,63,60,58,54,44,43,32,28,26,25,22,15,13,12,10,8,7,5,4,3], 'expected':False},
                      )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                self.assertEquals(x['expected'],solution.validMountainArray(x['array']))
