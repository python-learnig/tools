import unittest as test

class Solution:
    def moveZeroes(self, nums):
        writePoint = 0
        readPoint = 0
        while(readPoint<len(nums)):
            if(nums[readPoint] == 0):
                readPoint +=1
            else:
               temp = nums[readPoint]
               nums[readPoint]=0
               nums[writePoint] = temp
               readPoint+=1
               writePoint+=1



class TestSolution(test.TestCase):
    def test_moveZeroes(self):
        self.cases = ({'arr':[0,1,0,3,12], 'expect':[1,3,12,0,0]}, {'arr':[1], 'expect':[1]})
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.moveZeroes(x['arr'])
                self.assertEquals(x['expect'], x['arr'])
