import unittest as test

class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if(len(nums)== 0):
            return 0
        comparisonIndex = 0
        comparisonValue = nums[comparisonIndex]
        comparisonIndex +=1
        originalLength = len(nums)
        j = 1
        while(comparisonIndex < originalLength):
            if(comparisonValue == nums[comparisonIndex]):
                comparisonIndex+=1
            else:
                nums[j] = nums[comparisonIndex]
                comparisonValue = nums[j]
                j+=1
                comparisonIndex+=1
        return j

class TestSolution(test.TestCase):
    def test_removeDuplicates(self):
        self.cases=(
                    {'nums':[1,1,2], 'expected':[1,2,2]},
                    {'nums':[0,0,1,1,1,2,2,3,3,4], 'expected':[0,1,2,3,4, 2, 2, 3, 3, 4]},
                    {'nums':[], 'expected':[]}
        )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.removeDuplicates(x['nums'])
                self.assertEquals(x['expected'], x['nums'])

