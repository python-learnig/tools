import unittest as test

class Solution(object):
    def removeDuplicates(self, nums):
        writerPoint = 1
        for readerPoint in range(1,len(nums)):
            if(nums[readerPoint] != nums[readerPoint-1]):
                nums[writerPoint] = nums[readerPoint]
                writerPoint+=1
        return writerPoint

class TestSolution(test.TestCase):
    def test_removeDuplicates(self):
        self.cases=(
                    {'nums':[1,1,2], 'expected':[1,2,2]},
                    {'nums':[0,0,1,1,1,2,2,3,3,4], 'expected':[0,1,2,3,4, 2, 2, 3, 3, 4]},
                    {'nums':[], 'expected':[]}
        )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.removeDuplicates(x['nums'])
                self.assertEquals(x['expected'], x['nums'])

