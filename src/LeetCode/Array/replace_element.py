import unittest as test

class Solution(object):
    def replaceElements(self, arr):
        max = arr[len(arr)-1]
        arr[len(arr)-1] = -1
        i = len(arr)-2
        while(i >=0):
            if( arr[i] > max ):
                nextMax = arr[i]
                arr[i] = max
                max = nextMax
            else:
                arr[i] = max
            i = i -1
        return arr





class TestSolution(test.TestCase):
    def test_replaceElements(self):
        self.cases = (
            {'arr':[17,18,5,4,6,1], 'expected':[18,6,6,6,1,-1]},
                      )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.replaceElements(x['arr'])
                self.assertEquals(x['arr'], x['expected'])


