# https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3260/
import unittest as test
class Solution:
    def sortArrayByParity(self, nums):
        writePoint = 0
        readPoint = 0
        while(readPoint<len(nums)):
            if((nums[readPoint] % 2) == 1):
                readPoint +=1
            else:
               temp = nums[readPoint]
               nums[readPoint]=nums[writePoint]
               nums[writePoint] = temp
               readPoint+=1
               writePoint+=1
        return nums

class Solution:
    def sortArrayByParity(self, nums):
        i,j = 0, len(nums)-1
        while i<j:
            if nums[i]%2 > nums[j]%2:
                nums[i],nums[j] = nums[j], nums[i]
            if nums[i]%2 == 0:
                i+=1
            if nums[j]%2 == 1:
                j-=1
        return nums


class TestSolution(test.TestCase):
    def test_sortArrayByParity(self):
        self.cases = ({'arr':[3,1,2,4], 'expect':[2,4,3,1,]}, )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                solution.sortArrayByParity(x['arr'])
                self.assertEquals(x['expect'], x['arr'])
