#https://leetcode.com/explore/learn/card/fun-with-arrays/523/conclusion/3231/
import unittest as test
import sys

class Solution(object):
    def thirdMax(self, nums):
        thirdNumbers = []
        length = len(nums)
        i = 0
        maxValue = - sys.maxsize
        while i < length:
            if(self.fitNumber(nums[i], thirdNumbers) ):
                maxValue = nums[i] 
                thirdNumbers.append(maxValue)
                thirdNumbers.sort()
                self.checkNumbers(thirdNumbers)
            i+=1
        thirdNumbers.sort()
        if(len(thirdNumbers)<3):
            return thirdNumbers.pop(-1)
        return thirdNumbers.pop(0)
    
    def checkNumbers(self, thirdNumbers):
        if(len(thirdNumbers) > 3):
            thirdNumbers.pop(0)

    def fitNumber(self, value, thirdNumbers):
        if(value in thirdNumbers):
            return False
        if(len(thirdNumbers)< 3):
            return True
        length = len(thirdNumbers)
        i = 0
        while(i< length):
            if (thirdNumbers[i] < value):
                return True
            i+=1






class TestSolution(test.TestCase):
    def test_thirdMax(self):
        self.cases = (
            {'arr':[3,2,1], 'expected':1},
            {'arr':[1,2], 'expected':2},
            {'arr':[2,2,3,1], 'expected':1},
            {'arr':[1,2,2,5,3,5], 'expected':2},
            {'arr':[1,2,3,4,5,6], 'expected':4},

        )
        for x in self.cases:
            with self.subTest(case=x):
                solution = Solution()
                self.assertEquals(x['expected'],solution.thirdMax(x['arr']))

