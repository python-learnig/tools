# https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1212/

import unittest as test

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        slowRunner = head
        fastRunner = head
        while(slowRunner):
            index = 0
            while(fastRunner.next):
                index+=1
                if(index % 3 == 0):
                    slowRunner = slowRunner.next
                fastRunner = fastRunner.next
                if(slowRunner == fastRunner):
                    return True
            slowRunner = slowRunner.next

        return False


class TestSolution(test.TestCase):
    def test_1(self):
        nodeList = self.init([3,2,0,-4], 1)
        solution = Solution()
        self.assertTrue(solution.hasCycle(nodeList))
    def test_2(self):
        nodeList = self.init([3,2,0,-4], -1)
        solution = Solution()
        self.assertFalse(solution.hasCycle(nodeList))

    def init(self, nodes, param1):
       rootNode = None
       nextNode = None
       paramNode = None
       index = 0
       for nodeVal in nodes:
           node = ListNode(nodeVal)
           if(rootNode == None):
               rootNode = node
               continue
           if(nextNode == None):
               nextNode = node
               rootNode.next = nextNode
           else:
               nextNode.next = node
               nextNode = node
           if(param1 == index):
               paramNode = node
           index +=1

       nextNode.next = paramNode
       return rootNode



