#https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1214/
# Definition for singly-linked list.
import unittest as test

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        visited = set()
        runner = head
        while(runner):
            if runner in visited:
                return runner
            visited.add(runner)
            runner = runner.next

        return ListNode()




class TestSolution(test.TestCase):
    def test_1(self):
        nodeList = self.init([3,2,0,-4], 1)
        solution = Solution()
        self.assertEqual(2, solution.detectCycle(nodeList).val)
    def test_2(self):
        nodeList = self.init([3,2,0,-4], -1)
        solution = Solution()
        self.assertEqual(-1, solution.detectCycle(nodeList))
    def test_3(self):
        solution = Solution()
        self.assertEqual(-1, solution.detectCycle(ListNode(1)))

    def init(self, nodes, param1):
       rootNode = None
       nextNode = None
       paramNode = None
       index = 0
       for nodeVal in nodes:
           node = ListNode(nodeVal)
           if(rootNode == None):
               rootNode = node
               continue
           if(nextNode == None):
               nextNode = node
               rootNode.next = nextNode
           else:
               nextNode.next = node
               nextNode = node
           if(param1 == index):
               paramNode = node
           index +=1

       nextNode.next = paramNode
       return rootNode
