# https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1296/
# Definition for singly-linked list.
import unittest as test

class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution(object):
    def removeNthFromEnd(self, head, n):
        length = self.getLength(head)
        deletedIndex = length - n
        index = 1
        node = head
        if deletedIndex == 0:
            node = head.next
        while(head):
            next = head.next
            if index == deletedIndex:
                next = next.next
                head.next = next
            index += 1
            head = head.next
        return node


    def getLength(self, headA):
        index = 1
        while(headA.next):
            index+=1
            headA = headA.next
        return  index


class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if(nextNode):
            nextNode.next = paramNode
        return rootNode
    def reInit(self, head):
        list = []
        while(head):
            list.append(head.val)
            head = head.next
        return list

    def test_reeInit(self):
        head = self.init([3, 2, 0, -4])
        self.assertEquals([3, 2, 0, -4], self.reInit(head))

    def test_reInit177(self):
        head = self.init([1])
        solution = Solution()
        head = solution.removeNthFromEnd(head, 1)
        self.assertEquals([], self.reInit(head))

    def test_removeNthFromEnd(self):
        head = self.init([3,2,0,-4])
        solution = Solution()
        head = solution.removeNthFromEnd(head,1)
        reInitedList = self.reInit(head)
        self.assertEquals([3,2,0], reInitedList)


