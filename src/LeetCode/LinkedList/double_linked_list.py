#https://leetcode.com/explore/learn/card/linked-list/210/doubly-linked-list/1294/

import unittest as test
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Node:
    def __init__(self, val=None, next=None, prev=None):
        self.value = val
        self.next = next
        self.prev = prev

    def val(self):
        return self.value

    def setNext(self, next = None):
        if(next == None):
            return self.next
        self.next = next

    def setPrev(self, prev=None):
        if(prev == None):
            return self.prev
        self.prev = prev

class MyLinkedList(object):

    def __init__(self):
        self.index = -1
        self.head = None

    def get(self, index):
        counter = -1
        currentNode = self.head
        foundNode = None
        while(currentNode):
            counter+=1
            if(index == counter):
                foundNode = currentNode
                break
            currentNode = currentNode.next

        if(foundNode):
            return foundNode.val()
        else:
            return -1

    def addAtHead(self, val):
        if(self.head == None):
            self.head = Node(val)
        else:
            newHead = Node(val, self.head, None)
            self.head.setPrev(newHead)
            self.head = newHead

    def addAtTail(self, val):
        current = self.head
        last = current
        if(current == None):
            self.addAtHead(val)
            return
        while(current):
            last = current
            current = current.next

        newLast = Node(val,None, last)
        last.setNext(newLast)


    def addAtIndex(self, index, val):
        nodeData = self.getNode(index)
        currentNode = nodeData['currentNode']
        currentPoint = nodeData['currentPoint']
        if(index == 0):
            self.addAtHead(val)
            return
        if(currentNode == None and currentPoint == index):
           self.addAtTail(val)
           return
        if(currentNode == None and currentPoint< index):
            return
        if(index > currentPoint+1):
           return
        newCurrentNode = Node(val, currentNode, currentNode.prev)
        if(currentNode.prev):
            currentNode.prev.next = newCurrentNode
        currentNode.prev = newCurrentNode

    def getNode(self, index):
        currentPoint = 0
        currentNode = self.head
        if(index==0):
            return {'currentPoint': currentPoint, 'currentNode': currentNode}
        while(currentNode):
            if(currentPoint == index):
                break
            currentNode = currentNode.next
            currentPoint +=1
        return {'currentPoint': currentPoint, 'currentNode': currentNode}


    def deleteAtIndex(self, index):
        deletedNode = self.getNode(index)
        deletedNode = deletedNode['currentNode']

        if(deletedNode):
            prevNode = deletedNode.prev
            nextNode = deletedNode.next
        else:
            return -1
        if(prevNode):
            prevNode.next = nextNode
        if(nextNode):
            nextNode.prev = prevNode
        if(index == 0):
            self.head = nextNode






class TestSolution(test.TestCase):
    def testInit(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(12)
        self.assertEquals(12, linkedList.get(0))
        linkedList.addAtHead(13)
        self.assertEquals(13, linkedList.get(0))
    def testAddAtTail(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(12)
        linkedList.addAtTail(13)
        self.assertEquals(13, linkedList.get(1))

    def testAddAtIndex(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(1)
        linkedList.addAtTail(3)
        linkedList.addAtIndex(1, 2)
        self.assertEquals(2, linkedList.get(1))
#         test insert to last position
        linkedList.addAtIndex(3,4)
        self.assertEquals(4, linkedList.get(3))
#         test add to not exists list
        linkedList.addAtIndex(100,4)
        self.assertEquals(4, linkedList.get(3))
    def testDeleteAtIndex(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(1)
        linkedList.addAtTail(3)
        linkedList.addAtIndex(1, 2)
        self.assertEquals(2, linkedList.get(1))
        linkedList.deleteAtIndex(1)
        self.assertEquals(3, linkedList.get(1))

    def testCase_1(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(1)
        linkedList.deleteAtIndex(0)
    def testCase_2(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(7)
        linkedList.addAtHead(2)
        linkedList.addAtHead(1)
        linkedList.addAtIndex(3,0)
        linkedList.deleteAtIndex(2)
        linkedList.addAtHead(6)
        linkedList.addAtTail(4)
        linkedList.get(4)
        linkedList.addAtHead(4)
        linkedList.addAtIndex(5,0)
        linkedList.addAtHead(6)
    def testCase_3(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(1)
        linkedList.addAtTail(3)
        linkedList.addAtIndex(1,2)
        linkedList.get(1)
        linkedList.deleteAtIndex(0)
        self.assertEquals(2,linkedList.get(0))
    def testCase_4(self):
        linkedList = MyLinkedList()
        linkedList.addAtIndex(0,10)
        linkedList.addAtIndex(0,20)
        self.assertEqual(10, linkedList.get(1))
        linkedList.addAtIndex(1,30)
        self.assertEqual(20, linkedList.get(0))
        self.assertEqual(10, linkedList.get(2))
    def testCase_5(self):
        linkedList = MyLinkedList()
        linkedList.addAtTail(1)
        self.assertEquals(1, linkedList.get(0))

    def testCase_6(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(2)
        linkedList.deleteAtIndex(1)
        linkedList.addAtHead(20)
        linkedList.addAtHead(7)
        linkedList.addAtHead(3)
        linkedList.addAtHead(200)
        linkedList.addAtHead(5)
        linkedList.addAtTail(5)
        self.assertEquals(2, linkedList.get(5))
        linkedList.deleteAtIndex(6)
        linkedList.deleteAtIndex(4)

    def testCase_7(self):
        operations = ["addAtHead","addAtTail","addAtTail","get","get","addAtTail","addAtIndex","addAtHead","addAtHead","addAtTail","addAtTail","addAtTail","addAtTail","get","addAtHead","addAtHead","addAtIndex","addAtIndex","addAtHead","addAtTail","deleteAtIndex","addAtHead","addAtHead","addAtIndex","addAtTail","get","addAtIndex","addAtTail","addAtHead","addAtHead","addAtIndex","addAtTail","addAtHead","addAtHead","get","deleteAtIndex","addAtTail","addAtTail","addAtHead","addAtTail","get","deleteAtIndex","addAtTail","addAtHead","addAtTail","deleteAtIndex","addAtTail","deleteAtIndex","addAtIndex","deleteAtIndex","addAtTail","addAtHead","addAtIndex","addAtHead","addAtHead","get","addAtHead","get","addAtHead","deleteAtIndex","get","addAtHead","addAtTail","get","addAtHead","get","addAtTail","get","addAtTail","addAtHead","addAtIndex","addAtIndex","addAtHead","addAtHead","deleteAtIndex","get","addAtHead","addAtIndex","addAtTail","get","addAtIndex","get","addAtIndex","get","addAtIndex","addAtIndex","addAtHead","addAtHead","addAtTail","addAtIndex","get","addAtHead","addAtTail","addAtTail","addAtHead","get","addAtTail","addAtHead","addAtTail","get","addAtIndex"]
        data = [[84],[2],[39],[3],[1],[42],[1,80],[14],[1],[53],[98],[19],[12],[2],[16],[33],[4,17],[6,8],[37],[43],[11],[80],[31],[13,23],[17],[4],[10,0],[21],[73],[22],[24,37],[14],[97],[8],[6],[17],[50],[28],[76],[79],[18],[30],[5],[9],[83],[3],[40],[26],[20,90],[30],[40],[56],[15,23],[51],[21],[26],[83],[30],[12],[8],[4],[20],[45],[10],[56],[18],[33],[2],[70],[57],[31,24],[16,92],[40],[23],[26],[1],[92],[3,78],[42],[18],[39,9],[13],[33,17],[51],[18,95],[18,33],[80],[21],[7],[17,46],[33],[60],[26],[4],[9],[45],[38],[95],[78],[54],[42,86]]
        expected = [None,None,None,-1,2,None,None,None,None,None,None,None,None,84,None,None,None,None,None,None,None,None,None,None,None,16,None,None,None,None,None,None,None,None,37,None,None,None,None,None,23,None,None,None,None,None,None,None,None,None,None,None,None,None,None,19,None,17,None,None,56,None,None,31,None,17,None,12,None,None,None,None,None,None,None,40,None,None,None,37,None,76,None,42,None,None,None,None,None,None,80,None,None,None,None,43,None,None,None,40,None]
        self.assertData(operations, data,expected)

    def testCase_8(self):
        operations = ['addAtHead', 'addAtTail', 'addAtTail', 'get', 'get','get', 'addAtTail', 'addAtIndex', 'get', 'get',
                      'addAtHead', 'addAtHead','get', 'get', 'addAtTail', 'addAtTail', 'addAtTail', 'addAtTail', 'get', 'addAtHead', 'addAtHead', 'addAtIndex', 'get', 'get','get','get','get',
                      'addAtIndex', 'addAtHead', 'addAtTail', 'deleteAtIndex','get','get', 'addAtHead', 'addAtHead', 'addAtIndex','addAtTail',
                      'get','get', 'get', 'get','get',  #get 1
                      'addAtIndex', 'addAtTail',
                      'get', 'get', 'get', 'get', 'get', 'get', #get 2
                      'addAtTail', 'addAtHead', 'addAtTail',
                      'get', 'get', 'get', 'get',  #get 3
                      'deleteAtIndex', 'addAtTail', 'addAtHead',
                      'get', 'get', 'get', 'get', 'get', 'get',  # get 4
                      'addAtTail', 'deleteAtIndex', 'addAtTail',
                      'get', 'get', 'get', 'get', 'get', 'get', # get 5 [23:17], [20:19]
                      'deleteAtIndex', 'addAtIndex', 'deleteAtIndex',
                      'get', 'get', 'get','get','get', 'get', 'get','get', 'get', 'get', 'get', 'get', # get 6
                      'addAtTail', 'addAtHead', 'addAtIndex', 'addAtHead', 'addAtHead',
                      'get', 'get', 'get', 'get', 'get', 'get', 'get', #get 7 [26:17], [22:19], [5:31],
                      'get', 'addAtHead', 'get', 'addAtHead', 'deleteAtIndex', 'get', 'get',
                      'get', 'get','get', 'get', 'get','get', # get 8 [34:40], [33:40],[27:17], [23:19], [7:31]
                      'get', 'addAtHead', 'addAtTail', 'get', 'addAtHead',
                      'get', 'addAtTail', 'get', 'addAtTail', 'addAtHead',
                      ]
        data = [[84], [2], [39], [3], [1],[2], [42], [1, 80], [3], [4],
                [14], [1],[5],[6], [53], [98], [19], [12], [2], [16], [33], [4, 17],[10],[11],[12],[13],[14],
                [6,8], [37], [43], [11],[15],[14], [80], [31], [13, 23], [17],
                [19],[18], [17], [16], [0], #get 1 [19:17], [16:19], [0:31]
                [10, 0],[21],
                [21],[20],[22],[20],[17],[0], #get 2 [20:17], [17:19],[0:31]
                [28], [76], [79],
                [24],[23],[22],[1],#get 3 [21:17], [18:19], [1:31]
                [30], [5], [9],
                [26], [25], [24], [22], [19],[2], #get 4 [27:40],[22:17], [19:19], [2:31]
                [83], [3], [40],
                [27], [26], [25],[21], [18],[2], # get 5 [26:40],[21:17], [18:19],[2:31]
                [26], [20, 90], [30],
                [27], [26], [25],[24],[23],[22],[21],[20], [28], [22], [18], [2], # get 6, [27:40],[22:17], [18:19], [2:31]
                [40], [56], [15, 23], [51], [21],
                [32],[31],[30],[33],[26],[22], [5], #get 7 [32:40], [31:40],[26:17], [22:19], [5:31]
                [26], [83], [30], [12], [8], [27], [23],
                [33], [32], [27], [23], [30], [7], #get 8 [33:40], [32:40], [27:17], [23:19],[7:31],
                [4], [20], [45], [8], [56],
                [18], [33], [2], [70], [57]
                ]
        expected = [None, None, None, -1, 2,39,  None, None, 39, 42,
                    None, None,39,42, None, None, None, None, 84, None, None, None,53, 98, 19,12,-1,
                    None, None, None, None, 43,12,None, None, None, None,
                    17, 43, 12, 19, 31, #get 1
                    None,None,
                    21, 17, -1,  17, 19, 31, #get 2
                    None, None, None,
                    79, 28, 21,31, #get 3
                    None, None, None,
                    5, 79, 28, 17, 19, 31, #get 4
                    None, None, None,
                    40, 83, 5,17, 19,31, #get 5,
                    None, None, None,
                    40, 5, 79,28,21,17,43,90,-1, 17, 19, 31, #get 6
                    None, None, None, None, None,
                    40, 40, 5,-1, 17, 19, 31,  #get 7 [26:17], [22:19]
                    17, None, 79, None, None, 17, 19,
                    40, 40, 17, 19, 79, 31, # get 8 [33:40], [32:40], [27:17], [23:19],
                    56, None, None, 31, None,
                    17, None, 12, None, None
                    ]
        self.assertData(operations, data, expected)

    def test_case_62(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(1)
        linkedList.addAtTail(3)
        linkedList.addAtIndex(3,2)



    def assertData(self,  operations, data, expected):
        linkedList = MyLinkedList()
        numberOfOperation = -1
        for operation in operations:
            value = data.pop(0)
            expectedValue = expected.pop(0)
            numberOfOperation += 1
            if (numberOfOperation == 42):
                print(numberOfOperation)
            if operation == 'addAtIndex':
                getattr(linkedList, operation)(value[0], value[1])
                print('Operation:', numberOfOperation, '-', operation, 'value', value[0], 'value2', value[1],
                      'numberInList', linkedList.index)
                continue
            result = getattr(linkedList, operation)(value[0])
            try:
                self.assertEqual(expectedValue, result)
                print('Operation:', numberOfOperation, '-', operation, 'value', value[0], 'numberInList', linkedList.index, 'result', result)
            except:
                print( bcolors.WARNING,'expectedValue:', expectedValue, 'actualValue:', result, value, bcolors.ENDC)

    def testCase_add_at_tail(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(4)
        linkedList.addAtTail(5)
        linkedList.addAtTail(6)
        linkedList.addAtTail(7)
        linkedList.addAtTail(8)
        linkedList.addAtTail(9)
        linkedList.addAtTail(10)
        self.assertEquals(10, linkedList.get(6))
        self.assertEquals(9, linkedList.get(5))
        self.assertEquals(8, linkedList.get(4))
        self.assertEquals(7, linkedList.get(3))
        self.assertEquals(6, linkedList.get(2))
        self.assertEquals(5, linkedList.get(1))
        self.assertEquals(4, linkedList.get(0))

    def testCase_add_at_head(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(4)
        self.assertEquals(4, linkedList.get(0))


