# https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1215/
import unittest as test
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        smaller = self.getSmaller(headA, headB)
        bigger = self.getBigger(headA, headB)
        lengthB = self.getLength(bigger)
        lengthS = self.getLength(smaller)
        while(lengthB != lengthS):
            bigger = self.step(bigger)
            lengthB -=1
        while(lengthB >0):
            smaller = self.step(smaller)
            bigger = self.step(bigger)
            lengthB -=1
            if(smaller == bigger):
                return bigger
        return None

    def step(self, nodeList):
        if(nodeList and nodeList.next):
            return nodeList.next
        return None

    def getSmaller(self, headA, headB):
        lengthA = self.getLength(headA)
        lengthB = self.getLength(headB)
        if lengthB < lengthA:
            return headB
        return headA

    def getBigger(self,  headA,headB):
        lengthA = self.getLength(headA)
        lengthB = self.getLength(headB)
        if lengthB >= lengthA:
            return headB
        return headA


    def getLength(self, headA):
        index = 1
        while(headA.next):
            index+=1
            headA = headA.next
        return  index




class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1

        nextNode.next = paramNode
        return rootNode

    def test_length(self):
        nodeList = self.init([3,2,0,-4])
        solution = Solution()
        self.assertEqual(4, solution.getLength(nodeList))
        nodeList = self.init([1,2])
        self.assertEqual(2,solution.getLength(nodeList))
    def test_solution(self):
        headA = self.init([1,9,1,2,4])
        headB = self.init([3,2,4])
        solution = Solution()
        self.assertEqual(2, solution.getIntersectionNode(headA,headB))
        headA = self.init([2,6,4])
        headB = self.init([1,5])
        self.assertEqual(None, solution.getIntersectionNode(headA,headB))
        headA = self.init([4,1,8,4,5])
        headB = self.init([5,6,1,8,4,5])
        self.assertEqual(1, solution.getIntersectionNode(headA,headB))

    def test_smaller(self):
        solution = Solution()
        smaller = self.init([3,2,0,-4])
        bigger = self.init([2,3,4,5,6,7])
        self.assertEqual(smaller, solution.getSmaller(smaller,bigger))
        self.assertEqual(smaller, solution.getSmaller(bigger, smaller))

    def test_bigger(self):
        solution = Solution()
        smaller = self.init([3,2,0,-4])
        bigger = self.init([2,3,4,5,6,7])
        self.assertEqual(bigger, solution.getBigger(smaller,bigger))
        self.assertEqual(bigger, solution.getBigger(bigger, smaller))

