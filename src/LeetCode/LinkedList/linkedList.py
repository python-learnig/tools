# https://leetcode.com/explore/learn/card/linked-list/209/singly-linked-list/1290/
import unittest as test

class Node:
    def __init__(self, val, next):
        self.value = val
        self.nextValue = next

    def val(self):
        return self.value

    def next(self):
        return self.nextValue


class MyLinkedList(object):

    def __init__(self):
        self.head = None
        """
        Initialize your data structure here.
        """

    def get(self, index):
        """
        Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        :type index: int
        :rtype: int
        """
        node = self.getNodeByIndex(index)
        if(node != None):
            return node.val()
        return -1

    def getNodeByIndex(self, index):
        i = 0
        node = self.head
        while(i<index and node != None and node.next() != None):
            node = node.next()
            i = i+1
        if(i < index):
            return None
        return node


    def addAtHead(self, val):
        """
        Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
        :type val: int
        :rtype: None
        """
        self.head = Node(val,self.head)

    def addAtTail(self, val):
        """
        Append a node of value val to the last element of the linked list.
        :type val: int
        :rtype: None
        """
        last = self.getLast()
        if(last == None):
            self.head = Node(val, None)
        else:
            last.nextValue = Node(val, None)
    def getLast(self):
        node = self.head
        while(node != None):
            if(node.next() ==  None):
                break
            node = node.next()
        return node


    def addAtIndex(self, index, val):
        """
        Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted.
        :type index: int
        :type val: int
        :rtype: None
        """
        previousIndex = index -1
        if(index == 0):
            self.head = Node(val, self.head)
            return
        length = self.getLength()
        if(length == index):
            return self.addAtTail(val)
        if(index>length):
            return
        node = self.getNodeByIndex(previousIndex)
        if( node != None and node.next() != None ):
           nodeNext = node.next()
           node.nextValue = Node(val, nodeNext)

    def getLength(self):
        node = self.head
        length = 0
        while (node != None):
            length = length +1
            if (node.next() == None):
                break
            node = node.next()
        return length


    def deleteAtIndex(self, index):
        """
        Delete the index-th node in the linked list, if the index is valid.
        :type index: int
        :rtype: None
        """
        node = self.getNodeByIndex(index)
        if(index == 0):
            self.head = self.head.next()
            return
        if(node !=None):
            previousNode = self.getNodeByIndex(index -1)
            previousNode.nextValue = node.next()


# Your MyLinkedList object will be instantiated and called as such:
# obj = MyLinkedList()
# param_1 = obj.get(index)
# obj.addAtHead(val)
# obj.addAtTail(val)
# obj.addAtIndex(index,val)
# obj.deleteAtIndex(index)

# Your MyLinkedList object will be instantiated and called as such:
# obj = MyLinkedList()
# param_1 = obj.get(index)
# obj.addAtHead(val)
# obj.addAtTail(val)
# obj.addAtIndex(index,val)
# obj.deleteAtIndex(index)

class TestSolution(test.TestCase):
    def test_addAtHead(self):
        linkedList = MyLinkedList()
        self.assertIsNone(linkedList.addAtHead(1))
        self.assertIsNone(linkedList.addAtTail(3))
        self.assertIsNone(linkedList.addAtIndex(1,2))
        self.assertEquals(2, linkedList.get(1))
        self.assertIsNone(linkedList.deleteAtIndex(1))
        self.assertEquals(3, linkedList.get(1))
    def test_deleteUnExistsNode(self):
        linkedList = MyLinkedList()
        self.assertIsNone(linkedList.addAtHead(1))
        self.assertIsNone(linkedList.deleteAtIndex(0))

    def test_case_one(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(7)
        linkedList.addAtHead(2)
        linkedList.addAtHead(1)
        linkedList.addAtIndex(3,0)
        linkedList.deleteAtIndex(2)
        linkedList.addAtHead(6)
        linkedList.addAtTail(4)
        linkedList.get(4)
        linkedList.addAtHead(4)
        linkedList.addAtIndex(5,0)
        linkedList.addAtHead(6)

    def test_case_two(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(7)
        linkedList.addAtHead(2)
        linkedList.addAtHead(1)
        linkedList.addAtIndex(3,0)
        linkedList.deleteAtIndex(2)
        linkedList.addAtHead(6)
        linkedList.addAtTail(4)
        self.assertEquals(4,linkedList.get(4))
        linkedList.addAtHead(4)
        linkedList.addAtIndex(5,0)
        linkedList.addAtHead(6)

    def test_case_three(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(1)
        linkedList.addAtTail(3)
        linkedList.addAtIndex(1,2)
        self.assertEquals(2,linkedList.get(1))
        linkedList.deleteAtIndex(0)
        self.assertEquals(2,linkedList.get(0))

    def test_case_four(self):
        linkedList = MyLinkedList()
        linkedList.addAtIndex(0,10)
        linkedList.addAtIndex(0,20)
        linkedList.addAtIndex(1,30)
        self.assertEquals(20,linkedList.get(0))

    def test_case_58(self):
        linkedList = MyLinkedList()
        linkedList.addAtHead(2)
        linkedList.addAtIndex(0,1)
        self.assertEquals(2,linkedList.get(1))

