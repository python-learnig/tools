# https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1208/
# узнать длину списка, и вычислить количества итерация для перемещения,
# после этого идти по списку и менять местами данные
import unittest as test

class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution(object):
    def oddEvenList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        iterationNumber = self.getIterationNumber(head)
        index = 1
        prev = head
        while(index<iterationNumber):
            tmp = prev.next
            index +=1
            if(index % 2 == 0):
                prev.next = tmp.next
                lastElement = self.getLastElement(head)
                lastElement.next = tmp
                tmp.next = None
                index +=1
            prev = prev.next
        return head


    def getLastElement(self, head):
        while(head != None and head.next !=None):
            head = head.next
        return head

    def getIterationNumber(self, head):
        iterationNumber = 0
        while(head != None):
            iterationNumber +=1
            head = head.next
        return iterationNumber


class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if(nextNode):
            nextNode.next = paramNode
        return rootNode
    def reInit(self, head):
        list = []
        while(head):
            list.append(head.val)
            head = head.next
        return list
    def test_iterationNumber5(self):
        head = self.init([1, 2, 3, 4, 5])
        solution = Solution()
        self.assertEquals(5, solution.getIterationNumber(head))
    def test_iterationNumber4(self):
        head = self.init([1, 2, 3, 4])
        solution = Solution()
        self.assertEquals(4, solution.getIterationNumber(head))
    def test_iterationNumber0(self):
        head = self.init([])
        solution = Solution()
        self.assertEquals(0, solution.getIterationNumber(head))
    def test_getLastElement(self):
        head = self.init([1,2,3,4,5])
        solution = Solution()
        lastElement = solution.getLastElement(head)
        self.assertEquals(5, lastElement.val)

    def test_getLastElement4(self):
        head = self.init([1,2,3,4])
        solution = Solution()
        lastElement = solution.getLastElement(head)
        self.assertEquals(4, lastElement.val)
    def test_getLastElementNone(self):
        head = self.init([])
        solution = Solution()
        lastElement = solution.getLastElement(head)
        self.assertTrue(lastElement is None)


    def test_odd(self):
        head = self.init([1,2,3,4,5])
        solution = Solution()
        updatedList = solution.oddEvenList(head)
        self.assertEquals([1,3,5,2,4], self.reInit(updatedList))

    def test_odd_example(self):
        head = self.init([2,1,3,5,6,4,7])
        solution = Solution()
        updatedList =  solution.oddEvenList(head)
        self.assertEquals([2,3,6,7,1,5,4], self.reInit(updatedList))
