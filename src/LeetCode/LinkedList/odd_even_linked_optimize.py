# https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1208/
# узнать длину списка, и вычислить количества итерация для перемещения,
# после этого идти по списку и менять местами данные
import unittest as test

class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution(object):
    def oddEvenList(self, head):
        odds = ListNode(0)
        evens = ListNode(0)
        oddsHead = odds
        evensHead = evens
        isOdd = True
        while head:
            if isOdd:
                odds.next = head
                odds = odds.next
            else:
                evens.next = head
                evens = evens.next
            isOdd = not isOdd
            head = head.next
        evens.next = None
        odds.next = evensHead.next
        return oddsHead.next


    def getLastElement(self, head):
        iterationNumber = 0
        prev = head
        while(head != None):
            iterationNumber+=1
            prev = head
            head = head.next
        return (prev,iterationNumber)

#--------
class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if(nextNode):
            nextNode.next = paramNode
        return rootNode
    def reInit(self, head):
        list = []
        while(head):
            list.append(head.val)
            head = head.next
        return list

    def test_getLastElement(self):
        head = self.init([1,2,3,4,5])
        solution = Solution()
        lastElement = solution.getLastElement(head)
        self.assertEquals(5, lastElement.val)

    def test_getLastElement4(self):
        head = self.init([1,2,3,4])
        solution = Solution()
        lastElement,iterationNumber = solution.getLastElement(head)
        self.assertEquals(4, lastElement.val)
    def test_getLastElementNone(self):
        head = self.init([])
        solution = Solution()
        lastElement = solution.getLastElement(head)
        self.assertTrue(lastElement is None)


    def test_odd(self):
        head = self.init([1,2,3,4,5])
        solution = Solution()
        updatedList = solution.oddEvenList(head)
        self.assertEquals([1,3,5,2,4], self.reInit(updatedList))

    def test_odd_example(self):
        head = self.init([2,1,3,5,6,4,7])
        solution = Solution()
        updatedList =  solution.oddEvenList(head)
        self.assertEquals([2,3,6,7,1,5,4], self.reInit(updatedList))
    def test_another_example(self):
        head = self.init([1,1])
        solution = Solution()
        updatedList = solution.oddEvenList(head)
        self.assertEquals([1,1], self.reInit(updatedList))
