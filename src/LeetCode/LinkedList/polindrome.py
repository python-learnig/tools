# https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1209/
import unittest as test
import copy

class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution(object):
    def isPalindrome(self, head):
       numberAsc = self.getNumber(head)
       reverseList = SolutionReverse().reverseList(head)
       numberDesc = self.getNumber(reverseList)
       return numberAsc == numberDesc

    def getNumber(self, head):
        number = ''
        tmp = head
        while(tmp):
            number += str(tmp.val)
            tmp = tmp.next
        return number

class SolutionReverse:
    def reverseList(self, head):
        if not head or not head.next:
            return head
        tmp = None
        prev = None
        while head!=None:
            tmp = head.next
            head.next = prev
            prev = head
            head = tmp
        return prev


class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if (nextNode):
            nextNode.next = paramNode
        return rootNode

    def reInit(self, head):
        list = []
        while (head):
            list.append(head.val)
            head = head.next
        return list

    def test_polindrome(self):
        self.cases = (
            {'name':'[3,2] isn\'t polindrom','expected': False, 'list': [3,2]},
            {'name':'[3,3] is polindrom','expected': True, 'list': [3,3]},
            {'name':'[1,1,2,1]', 'expected':False, 'list':[1,1,2,1]},
        )
        for x in self.cases:
            with self.subTest(case=x['name']):
                head = self.init(x['list'])
                solution = Solution()
                self.assertEquals(x['expected'],solution.isPalindrome(head))

