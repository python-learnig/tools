import unittest as test
import copy

class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution(object):
    def isPalindrome(self, head):
        self.front_pointer = head
        return self.check_recursive(head)


    def check_recursive(self, current_point):
       if current_point.next != None:
           self.check_recursive(current_point.next)
       if self.front_pointer.val == current_point.val:
           self.front_pointer = self.front_pointer.next
           return True
       else:
           return False


class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if (nextNode):
            nextNode.next = paramNode
        return rootNode

    def reInit(self, head):
        list = []
        while (head):
            list.append(head.val)
            head = head.next
        return list

    def test_polindrome(self):
        self.cases = (
            {'name':'[3,2] isn\'t polindrom','expected': False, 'list': [3,2]},
            {'name':'[3,3] is polindrom','expected': True, 'list': [3,3]},
            {'name':'[1,1,2,1]', 'expected':False, 'list':[1,1,2,1]},
        )
        for x in self.cases:
            with self.subTest(case=x['name']):
                head = self.init(x['list'])
                solution = Solution()
                self.assertEquals(x['expected'],solution.isPalindrome(head))
