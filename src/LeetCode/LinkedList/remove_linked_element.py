# https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1207/
import unittest as test
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution(object):
    def removeElements(self, head, val):
       next = head
       prev = head
       while(next):
            if(next.val == val):
                if(next == head):
                    prev = next.next
                    head = prev
                else:
                    prev.next = next.next
            else:
                prev = next
            next = next.next
       return head


class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if(nextNode):
            nextNode.next = paramNode
        return rootNode
    def reInit(self, head):
        list = []
        while(head):
            list.append(head.val)
            head = head.next
        return list

    def test_removeLastElements(self):
        head = self.init([3, 2])
        solution = Solution()
        updatedList = solution.removeElements(head, 2)
        self.assertEquals([3], self.reInit(updatedList))
    def test_removeMiddleElement(self):
        head = self.init([3, 2, 3])
        solution = Solution()
        updatedList = solution.removeElements(head, 2)
        self.assertEquals([3,3], self.reInit(updatedList))

    def test_removeMiddleElements(self):
        head = self.init([3, 2, 2, 2, 2, 3])
        solution = Solution()
        updatedList = solution.removeElements(head, 2)
        self.assertEquals([3,3], self.reInit(updatedList))
    def test_removeFirstElements(self):
        head = self.init([4, 2, 2, 2, 2, 3])
        solution = Solution()
        updatedList = solution.removeElements(head, 4)
        self.assertEquals([2, 2, 2, 2, 3], self.reInit(updatedList))
    def test_removeAllElements(self):
        head = self.init([2, 2, 2, 2, 2, 2])
        solution = Solution()
        updatedList = solution.removeElements(head, 2)
        self.assertEquals([], self.reInit(updatedList))
    def test_emptyList(self):
        head = self.init([])
        solution = Solution()
        updatedList = solution.removeElements(head, 2)
        self.assertEquals([], self.reInit(updatedList))
    def test_removeAllMiddlesElements(self):
        head = self.init([4, 2, 2, 2, 2, 3])
        solution = Solution()
        updatedList = solution.removeElements(head, 2)
        self.assertEquals([4,3], self.reInit(updatedList))
