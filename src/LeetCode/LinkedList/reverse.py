#https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1205/
# Definition for singly-linked list.
import unittest as test
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def reverseList(self, head):
        if not head or not head.next:
            return head
        tmp = None
        prev = None
        while head!=None:
            tmp = head.next
            head.next = prev
            prev = head
            head = tmp
        return prev

class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if(nextNode):
            nextNode.next = paramNode
        return rootNode
    def reInit(self, head):
        list = []
        while(head):
            list.append(head.val)
            head = head.next
        return list

    def test_reeInit(self):
        head = self.init([3, 2])
        solution = Solution()
        reversedList = solution.reverseList(head)
        self.assertEquals([2, 3], self.reInit(reversedList))

    def test_3thelemeents(self):
        head = self.init([3, 2, 1])
        solution = Solution()
        reversedList = solution.reverseList(head)
        self.assertEquals([1, 2, 3], self.reInit(reversedList))




