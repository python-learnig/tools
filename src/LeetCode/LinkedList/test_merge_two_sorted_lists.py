import unittest as test
from merge_two_sorted_lists import Solution
from merge_two_sorted_lists import ListNode 

class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if(nextNode):
            nextNode.next = paramNode
        return rootNode
    def toArrayFromList(self,list):
        if(list==[]):
            return []
        newList = []
        while(list):
            newList.append(list.val)
            list = list.next
        return newList

    def test_createList(self):
        list1 = self.init([])
        list2=self.init([2])
        solution = Solution()
        mergedList = solution.mergeTwoLists(list1, list2)
        mergedArray = self.toArrayFromList(mergedList)
        self.assertEquals([2], mergedArray)

    def test_createNoneList2(self):
        list1 = self.init([2])
        list2=self.init([])
        solution = Solution()
        mergedList = solution.mergeTwoLists(list1, list2)
        mergedArray = self.toArrayFromList(mergedList)
        self.assertEquals([2], mergedArray)

    def test_createNoneBothList(self):
        list1 = self.init([])
        list2=self.init([])
        solution = Solution()
        mergedList = solution.mergeTwoLists(list1, list2)
        mergedArray = self.toArrayFromList(mergedList)
        self.assertEquals([], mergedArray)


    def test_createBothListSameLength(self):
        list1 = self.init([1])
        list2=self.init([2])
        solution = Solution()
        mergedList = solution.mergeTwoLists(list1, list2)
        mergedArray = self.toArrayFromList(mergedList)
        self.assertEquals([1,2], mergedArray)

    def test_createList1LongerLength(self):
        list1 = self.init([1,3])
        list2=self.init([2])
        solution = Solution()
        mergedList = solution.mergeTwoLists(list1, list2)
        mergedArray = self.toArrayFromList(mergedList)
        self.assertEquals([1,2,3], mergedArray)

    def test_createListSameLongerLength(self):
        list1 = self.init([1,3])
        list2=self.init([2,4])
        solution = Solution()
        mergedList = solution.mergeTwoLists(list1, list2)
        mergedArray = self.toArrayFromList(mergedList)
        self.assertEquals([1,2,3,4], mergedArray)

    def test_createList_2(self):
        list1 = self.init([1,3,5])
        list2=self.init([2,4])
        solution = Solution()
        mergedList = solution.mergeTwoLists(list1, list2)
        mergedArray = self.toArrayFromList(mergedList)
        self.assertEquals([1,2,3,4,5], mergedArray)
