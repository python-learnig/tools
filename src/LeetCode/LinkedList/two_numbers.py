# https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1228/
import unittest as test

class ListNode(object):
    def __init__(self, val=None, next=None):
        self.val = val
        self.next = next

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        
        return l1


class TestSolution(test.TestCase):
    def init(self, nodes):
        rootNode = None
        nextNode = None
        paramNode = None
        index = 0
        for nodeVal in nodes:
            node = ListNode(nodeVal)
            if (rootNode == None):
                rootNode = node
                continue
            if (nextNode == None):
                nextNode = node
                rootNode.next = nextNode
            else:
                nextNode.next = node
                nextNode = node
            index += 1
        if(nextNode):
            nextNode.next = paramNode
        return rootNode

    def reInit(self, head):
        list = []
        while (head):
            list.append(head.val)
            head = head.next
        return list

    def toArrayFromList(self,list):
        if(list==[]):
            return []
        newList = []
        while(list):
            newList.append(list.val)
            list = list.next
        return newList
    def add(self, l1, l2):
        solution = Solution()
        lR = solution.addTwoNumbers(l1,l2)
        return lR;
    def test_one_number_in_array(self):
        l1 = self.init([2])
        l2 = self.init([2])
        lR = self.add(l1,l2)
        self.assertEquals([4], self.reInit(lR))
    def test_one_two_number_in_array(self):
        l1 = self.init([2])
        l2 = self.init([2,1])
        lR = self.add(l1,l2)
        self.assertEquals([4,1], self.reInit(lR))


    def test_342_465(self):
        l1 = self.init([2,4,3])
        l2 = self.init([5,6,4])
        lR = self.add(l1,l2)
        self.assertEquals([7,0,8], self.reInit(lR))
