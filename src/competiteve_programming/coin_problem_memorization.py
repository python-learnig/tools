import math
coins = [1,2,3]
ready = {}
def solve(x):
    if(x<0):
        return math.inf
    if(x == 0):
        return 0
    if(x in ready):
        return ready[x]
    best = math.inf
    for i in coins:
        best = min(best, solve(x-i)+1)
    ready[x] = best
    return best

print(solve(500))
