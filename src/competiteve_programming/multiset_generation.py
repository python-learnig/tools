allSubsets = []
visit = {}
def generator(n, subset):
    if(len(subset) == n):
       allSubsets.append(subset[:])
    else:
        for i in range(n+1):
            if( i in visit and visit[i] == True):
                continue
            visit[i] = True
            subset.append(i)
            generator(n, subset)
            visit[i] = False
            subset.pop()

generator(2,[])
print(allSubsets)
