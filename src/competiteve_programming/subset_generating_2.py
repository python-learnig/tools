def generate(n):
    b = 0
    while(b<(1<<n)):
        subset = []
        i = 0
        while(i<n):
            if(b&(1<<i)):
                subset.append(i)
            i+=1
        print(subset)
        b+=1

generate(2)
