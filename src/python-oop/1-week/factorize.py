from contracts import contract
import unittest as test

def factorize(x:'int,>=0'):
    """ Factorize positive integer and return its factors.
        :type x: int,>=0
        :rtype: tuple[N],N>0
    """
    pass


class TestFactorize(test.TestCase):

    def test_wrong_types_raise_exception(self):
        self.cases = ({'value':1.5,'error':TypeError},
                      {'value': 'string', 'error': TypeError})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertRaises(x['error'], factorize, x['value'])
    def test_negative(self):
        self.cases = ({'value': -1, 'error': ValueError},
                      {'value': -10, 'error': ValueError},
                      {'value': -100, 'error': ValueError})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertRaises(x['error'], factorize, x['value'])
    def test_zero_and_one_cases(self):
        self.cases = ({'value': 0, 'expect': (0,)},
                      {'value': 1, 'expect': (1,)})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], factorize(x['value']))
    def test_simple_numbers(self):
        self.cases = ({'value': 3, 'expect': (3,)},
                      {'value': 13, 'expect': (13,)},
                      {'value': 29, 'expect': (29,)})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], factorize(b['value']))
    def test_two_simple_multipliers(self):
        self.cases = ({'value': 6, 'expect': (2, 3)},
                      {'value': 26, 'expect': (2, 13)},
                      {'value': 121, 'expect': (11,11)})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], factorize(x['value']))
    def test_many_multipliers(self):
        self.cases = ({'value': 1001, 'expect':  (7, 11, 13)},
                      {'value': 9699690, 'expect': (2, 3, 5, 7, 11, 13, 17, 19)})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], factorize(x['value']))



    def tearDown(self):
        self.cases = None

if __name__=='__main__':
    test.main()