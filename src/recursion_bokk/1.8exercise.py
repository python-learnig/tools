def sum_length(a, length):
    if(length <= 1):
        return a[length-1]
    return sum_length(a[0:length-1], length-1) + a[length-1]

# print(sum_length([0,1,2,3], 4))

def sum_length_b(a, length):
    if(length==1):
        return a[0]
    return a[0] + sum_length_b(a[1:length], length-1)

# print(sum_length_b([0,1,2,3], 4))

def generate(n):
    b = 0
    while(b<(1<<n)):
        subset = []
        i = 0
        while(i<n):
            if(b&(1<<i)):
                subset.append(i)
            i+=1
        print(subset)
        b+=1
generate(2)
