def print_digits_reversed_verically(n):
    if n < 10:
        print(n)
    else:
        print_digits_reversed_verically(n//10)
        print(n%10)
