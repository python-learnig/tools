def fib_1(n):
    F = 1.618
    if n == 1 or n == 2:
        return 1
    return (F*(fib_1(n-1))+1/2) //1

def fib_2_append(n, a, b):
    if n == 1:
        return b
    return fib_2_append(n-1,a+b, a)

def fib_2(n):
    a = 1
    b=n
    return fib_2_append(n,1,1)


def fib_3(n):
    if n == 1 or n == 2:
        return 1
    if(n % 2 == 0):
        return fib_3(n/2+1)**2 - fib_3(n/2 -1)**2
    else:
        return fib_3((n+1)/2)**2 + fib_3((n-1)/2)**2


def fib_4_A(n):
    if n == 1:
        return 0
    return fib_4_A(n-1) +  fib_4_B(n-1)


def fib_4_B(n):
    if n == 1:
        return 1
    return fib_4_A(n-1)

def fib_4_F(n):
    return fib_4_A(n)+fib_4_B(n)


def fib_5_extend(n, s):
    if(n == 1 or n == 2):
        return 1 + s
    return fib_5_extend(n-1, s + fib_5_extend(n-2,0))

def fib_5(n):
    s = 0
    return fib_5_extend(n,s)





