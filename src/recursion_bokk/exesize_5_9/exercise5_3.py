def calculate(a,b):
    if len(a) == 0:
        return b
    element = a.pop(0)
    b[element] +=1
    return calculate(a,b)

def prepareBeforeCalculation(a):
    b= [0]*(max(a)+1)
    return calculate(a,b)

def reformatCalculation(b,a,index = 0):
    if(len(b) ==0 ):
        return a
    element = b.pop(0)
    a = a + [index]*element
    index = index+1
    return reformatCalculation(b,a, index)



print(reformatCalculation(prepareBeforeCalculation([2,2,3,2,0,1,3,2,0,0,4]), [], 0))
