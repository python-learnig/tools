listOfRange = {0:'', 1:'',2:'\u00B2', 3:'\u00B3' }

def polinom_createater(polinoms):
    if len(polinoms) == 0:
            return ''
    prefix = ''
    if(polinoms[len(polinoms)-1] > 1):
        prefix = '+'+str(polinoms[len(polinoms)-1])
    if(polinoms[len(polinoms)-1] < -1):
        prefix = str(polinoms[len(polinoms)-1])
    value = 'x'
    if(polinoms[len(polinoms) -1 ] == 0 or len(polinoms)-1 ==0 ):
        value = ''
    postfix =   listOfRange[len(polinoms)-1]
    if (value == ''):
        postfix = ''
    strPolinom = prefix + value + postfix
    return strPolinom+polinom_createater(polinoms[:len(polinoms)-1])


print(polinom_createater([3,-5,0,1]))
print(polinom_createater([3,-5,0]))
