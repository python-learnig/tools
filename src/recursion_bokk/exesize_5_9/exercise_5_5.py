import unittest as test

def binary_detect(x, a):
    if(len(a)  == 0):
        return -1
    middle = len(a) // 2
    if(x == a[middle]):
        return 1
    if(len(a) == 1):
        return -1

    if(x > a[middle]):
        return binary_detect(x, a[middle:])
    return binary_detect(x, a[:middle])

class TestSolution(test.TestCase):
    def test_binary_detect(self):
        self.cases = (
            {
           'x':0,
           'a':[2,3,4,5],
           'expected':-1
        },
        {
           'x':2,
           'a':[2,3,4,5],
           'expected':1
        },
                      {
                          'x':5,
                          'a':[2,3,4,5],
                          'expected':1
                      },
                      {
                          'x': 50,
                          'a': [2, 3, 4, 5],
                          'expected': -1
                      },
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(binary_detect(x['x'], x['a']), x['expected'])
