import unittest as test

def detect_same_position(a, correction = 0):
         middle = len(a) // 2 + correction
         if(a[middle] == middle):
             return middle
         if(len(a) == 1):
             return -1
         if(a[middle] > middle):
             return detect_same_position(a, -(middle // 2))
         if(a[middle] < middle):
             return detect_same_position(a, middle // 2)

class TestSolution(test.TestCase):
    def test_detect(self):
        self.cases = (
            {
                'a':[-1,0,2,5],
                'expected': 2
            },
            {
                'a': [-3,-1,2,5,6,7,9],
                'expected': 2
            },
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(detect_same_position(x['a']), x['expected'])
