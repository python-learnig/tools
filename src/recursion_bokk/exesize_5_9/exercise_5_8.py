import unittest as test


def detect_odd_position(a, correction=0):
        last = len(a) - 1 - correction
        if(last <= 0):
            return -1
        if(a[last] % 2 ==0 ):
            return last
        return detect_odd_position(a, (correction + (len(a)-correction)//2) )

class TestSolution(test.TestCase):
    def test_detect_odd_position(self):
        self.cases = (
            {
                'a': [0, 2],
                'expected': 1
            },
            {
                'a': [1, 3],
                'expected': -1
            },
            {
                'a': [0,2,1, 3],
                'expected': 1
            }
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(detect_odd_position(x['a']), x['expected'])
