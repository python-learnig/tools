def nuton_func_calculation(x0, a, n):
    xn = x0 - x0 / 2 + a / (2 * x0)
    if(n ==1):
        return xn
    return nuton_func_calculation(xn, a, n-1)


print(nuton_func_calculation(12,5,3))
