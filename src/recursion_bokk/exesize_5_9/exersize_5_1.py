def isConsistOddNumber(number):
    if (number < 10):
        return (number%2) == 1
    value = number % 10
    if(value %2 == 1):
        return True
    return isConsistOddNumber(number // 10)

def linearIsConsistOddNumber(number):
    return (number % 2 == 1) or (number %10)%2 or isConsistOddNumber(number // 10)


print(isConsistOddNumber(2224))
print(linearIsConsistOddNumber(2224))
print(isConsistOddNumber(2324))
print(linearIsConsistOddNumber(2324))

