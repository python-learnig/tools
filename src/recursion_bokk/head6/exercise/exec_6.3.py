import unittest as test


def  getArraySum(a):
    if(len(a) == 1):
        return a[0]
    return a[0] + getArraySum(a[1:])

def getMaxConsequence(a):
    n = 0
    max=-123141
    while( n < len(a)):
        max = max(max, getArraySum(a[n:]))
    return max


class TestSolution(test.TestCase):
    def test_array_sum(self):
        a = [1,2,3,4,5]
        self.assertEqual(15, getArraySum(a))


a =[-1,-4,5,2,-3,4,2,-5]
print(getArraySum(a))
expected = [5,2,-3,4,2]

