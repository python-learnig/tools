def findElement(x, a):
    if(len(a) == 1):
        return a[0] == x
    n = len(a)
    return findElement(x,a[0:n//2]) or findElement(x,a[n//2:n])

print(findElement(10, [10,1,12,13,14]))



