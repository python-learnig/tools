import unittest as test


def  getArraySum(a,start, result = None):
    if(len(a) == 1):
        return a[0]
    if(start == len(a)):
        return 0
    if result == None:
        result = a[start]
    else:
        result += a[start]
    return max(result, a[start] + getArraySum(a, start+1))

def getMaxConsequence(a):
    n = 0
    maxValue = -100000
    while( n < len(a)):
        maxValue = max(maxValue, getArraySum(a, n))
        n = n + 1
    return maxValue


class TestSolution(test.TestCase):
    def test_array_sum(self):
        a = [1,2,3,4,5]
        self.assertEqual(12, getArraySum(a, 2))
    def test_array_sum_2(self):
        a = [-1,-4,5,2,-3,4,2,-5]
        self.assertEqual(5, getArraySum(a, 0))
    def test_array_sum_3(self):
        a = [-1,-4,5,2,-3,4,2,-5]
        self.assertEqual(10, getArraySum(a, 2))
    def test_max_consequence(self):
        a = [-1,-4,5,2,-3,4,2,-5]
        self.assertEqual(10, getMaxConsequence(a))


a =[-1,-4,5,2,-3,4,2,-5]
expected = [5,2,-3,4,2]

