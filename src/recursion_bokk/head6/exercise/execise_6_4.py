import unittest as test

def multiply_polinoms(a,b, sum = []):
    if(sum == []):
        sum = [0] * (len(a) + len(b) - 2 +1)
    if(len(a) == 1):
        return [a[0] * element for element in b]
    if(len(b) == 1):
        return [b[0] * element for element in a]

    dimension = (len(a) -1 ) + (len(b) - 1) +1
    x = a[0:len(a) // 2]
    y = a[len(a) // 2:]
    x1 = b[0:len(b) // 2]
    y1 = b[len(b) // 2:]
    c1 = multiply_polinoms(x, x1)
    c2 = multiply_polinoms(y, y1)
    c3 = multiply_polinoms(y, x1)
    c4 = multiply_polinoms(x, y1)
    index = 0

    for element in c1:
        sum[index] += element
        index += 1

    index = (dimension - 1)

    while(c2):
        sum[index] += c2.pop()
        index -= 1


    index = dimension - len(y) - len(x1)

    while(c3):
        sum[index] += c3.pop()
        index -= 1

    index = dimension - len(x) - len(y1)

    while(c4):
        sum[index] += c4.pop()
        index -= 1
    return sum



class TestSolution(test.TestCase):
    def test_array_sum_one_same_dimension(self):
        a = [1]
        b = [2]
        self.assertEqual([2], multiply_polinoms(a, b))
    def test_array_sum_one_two_dimension(self):
        a = [2]
        b = [2,3]
        self.assertEqual([4, 6], multiply_polinoms(a, b))

    def test_array_sum_two_two_zero_second_dimension(self):
        a = [2,3]
        b = [2,0]
        self.assertEqual([4,6,0], multiply_polinoms(a, b))

    def test_array_sum_two_two_dimension(self):
        a = [2,3]
        b = [2,4]
        self.assertEqual([4,14,12], multiply_polinoms(a, b))

    def test_array_sum_two_three_dimension(self):
        a = [2,3,4]
        b = [0,2,4]
        self.assertEqual([4,14,20,16], multiply_polinoms(a, b))

    def test_array_sum_three_three_dimension(self):
        a = [2,3,4]
        b = [2,4,5]
        self.assertEqual([4,14,30,31,20], multiply_polinoms(a, b))

    def test_array_sum_variant_one(self):
        a = [3,4]
        b = [4,5]
        self.assertEqual([12,31,20], multiply_polinoms(a, b))
