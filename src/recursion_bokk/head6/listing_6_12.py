import listing_6_11 as figure
def trominoes(x,y,n,p,q):
    if n == 2:
        if y == q:
            if x == p:
                figure.draw_L1(x,y)
            else:
                figure.draw_L2(x,y)
        else:
            if x == p:
                figure.draw_L3(x,y)
            else:
                figure.draw_L4(x,y)
    else:
        mid_x = x + n // 2
        mid_y = y + n // 2
        if q < mid_y:
            if p < mid_x:
                figure.draw_L1(mid_x -1, mid_y -1)
                trominoes(x,y,n // 2, p, q)
                trominoes(x, mid_y, n // 2, mid_x - 1, mid_y)
                trominoes(mid_x, y, n // 2, mid_x, mid_y -1)
                trominoes(mid_x, mid_y, n // 2, mid_x, mid_y)
            else:
                figure.draw_L2(mid_x -1, mid_y -1)
                trominoes(x,y,n // 2, mid_x -1, mid_y -1)
                trominoes(x, mid_y, n // 2, mid_x - 1, mid_y)
                trominoes(mid_x, y, n // 2, p, q)
                trominoes(mid_x, mid_y, n // 2, mid_x, mid_y)
        else:
            if p < mid_x:
                figure.draw_L3(mid_x - 1, mid_y - 1)
                trominoes(x, y, n // 2, mid_x - 1, mid_y - 1)
                trominoes(x, mid_y, n // 2, p, q)
                trominoes(mid_x, y, n // 2, mid_x, mid_y - 1)
                trominoes(mid_x, mid_y, n // 2, mid_x, mid_y)
            else:
                figure.draw_L4(mid_x - 1, mid_y - 1)
                trominoes(x, y, n // 2, mid_x - 1, mid_y - 1)
                trominoes(x, mid_y, n // 2, mid_x - 1, mid_y)
                trominoes(mid_x, y, n // 2, mid_x, mid_y - 1)
                trominoes(mid_x, mid_y, n // 2, p, q)


