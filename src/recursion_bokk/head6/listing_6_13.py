import random
import matplotlib.pyplot as plt
import listing_6_12 as draw_helper
from matplotlib.patches import Rectangle


fig =  plt.figure()
fig.patch.set_facecolor('white')
ax = plt.gca()
n = 4
p = random.choice([i for i in range(n)])
q = random.choice([i for i in range(n)])
p=1
q=1
ax.add_patch(Rectangle((p,q),1,1,facecolor=(0.5,0.5,0.5)))
draw_helper.trominoes(0,0,n,p,q)
plt.axis('equal')
plt.axis('off')
plt.show()
