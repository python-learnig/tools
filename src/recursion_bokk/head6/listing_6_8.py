import time

def number_of_bits(n):
    if(n < 2 ):
        return 1
    return 1 + number_of_bits(n >> 1)

def multiply_karatsuba(x,y):
    if x == 0 or y == 0:
        return 0
    if x == 1:
        return y

    if y == 1:
        return x
    else:
        n_bits_x = number_of_bits(x)
        n_bits_y = number_of_bits(y)
        m = min(n_bits_x // 2, n_bits_y // 2)
        a = x >> m
        b = x - (a << m)
        c = y >> m
        d = y - (c << m)

        ac = multiply_karatsuba(a,c)
        bd = multiply_karatsuba(b,d)

        t = multiply_karatsuba(a+b, c+d) - ac -bd
        return (ac << (2*m)) + (t << m) + bd

a = 113241235123512135125123512351251251321235123512351324123512351235123512351235123512351235213561261246136413461235123512351235123512351235123519238741293846123846129512983651
b = 121234123412341234345135151351235125123512351512351351235123512351235123556956080501246124614614613461346123412351235135135123512351235123512351235123512951209581209358129351251235
start = time.time()
result = multiply_karatsuba(a,b)
finish = time.time()
print(result, finish - start)
start = time.time()
result = a*b
finish = time.time()
print(result, finish - start)
