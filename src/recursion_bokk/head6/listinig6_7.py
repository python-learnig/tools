def majority_element_in_list(a):
    n = len(a)
    if(len(a) == 0):
        return (False, None, 0)
    elif n == 0:
        return (False, None, 0)
    else:
        a = a[0 : n//2]
        b = a[n//2 : n]
        t = majority_element_in_list(a)
        if t[0]:
            occurrences = occurences_in_list(b, t[1])
            if(t[2] + occurrences > n / 2):
                return (True, t[1], t[2]+occurrences)
        t = majority_element_in_list(b)

        if t[0]:
            occurrences = occurrences_in_list(a, t[1])
            if(t[2] + occurrences > n /2):
                return (True, t[1], t[2] + occurrences)

        return (False, None, 0)
