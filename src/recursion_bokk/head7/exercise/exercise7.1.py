def binominal(C,m,n):
    if m == 0 or m == n:
        return 1
    else:
        return binominal(C,m-1,n-1) + binominal(C,m,n-1)

print(binominal(10,5,10))
