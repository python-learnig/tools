import numpy as np
def exists_path_swap(A, r, c=0):
    if r < 0 or c >= A.shape[0] or A[c,r] == 'W':
        return False
    elif A.shape[1] == c+1:
        return True
    else:
        return (
            exists_path_swap(A, r-1, c+1)
            or exists_path_swap(A, r, c+1)
            or exists_path_swap(A, r+1,c+1)
        )

A = np.array([['W',1,1],
              [1,'W',1],
              [1,1,'W']])

print(exists_path_swap(A, 0))
