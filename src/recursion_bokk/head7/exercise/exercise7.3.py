import math


def get_extend(demension, i):
    if demension%i ==  0:
        return demension/i

    while i%2 == 0:
        demension = demension/2
        i = i/2
    return demension



def print_line(n,precise=1):
        demension = 2 ** precise
        j=0
        multipleZero = int(precise + 1 - j)
        print('-'*multipleZero+ str(n))
        for i in range(1,demension):
            extend = get_extend(demension, i)
            j = math.log2(extend)
            # print ('logaright',j, demension, i, int(j)==j)
            if(j == int(j) and j > 0):
                multiple = int(precise+1 - j)
                delimeter = '-' * multiple
                print(delimeter)
                continue
            print('-')

        if n == 0:
            return 0
        print_line(n-1, precise)

print(print_line(5, 3))
# надо найти на сколько двоек можно разделить i нацело и взять и разделить на это число делитель - demension.
