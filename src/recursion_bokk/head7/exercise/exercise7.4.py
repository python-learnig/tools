def hanoi(n, o, d, a):
    if(n==1):
        print('-Move disk from ',o,'to disk ',a)
        print('-Move disk from ',a,'to disk ',d)
    else:
        hanoi(n-1, o, d, a)
        print('--Move disk from ',o,'to disk ',a)
        hanoi(n-1, d, o, a)
        print('---Move disk from ',a,'to disk ',d)
        hanoi(n-1, o, d, a)




hanoi(3, 'O', 'D', 'A')
