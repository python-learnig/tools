def countBinTreeNode(node):
    if not node[2] and not node[3]:
        return 0
    return 1 + countBinTreeNode(node[2]) + countBinTreeNode(node[3])

node = ['Emma', '20002',['Anna','1999',[],[]],['Paul', '2000',['Lara','1987',['John','2006',[],[]],['Luke','1976',[],[]]],['Sara','1995',[],[]]]]

print(countBinTreeNode(node))
