def longest_substring(s):
    n = len(s)
    if is_the_same(s):
        return s
    else:
        s_aux_1 = longest_substring(s[1:n])
        s_aux_2 = longest_substring(s[0:n-1])
        if len(s_aux_1) > len(s_aux_2):
            return s_aux_1
        else:
            return s_aux_2

def is_the_same(s):
    if len(s) <= 1:
        return True
    else:
        return s[0] == s[-1] and s[0]==s[1] and is_the_same(s[1:-1])



print(longest_substring('1355444556'))
