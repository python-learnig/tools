import unittest as test

def the_longest_palindrome(s):
    n = len(s)
    if n <= 1:
        return s
    else:
        if s[0] == s[-1]:
            return s[0] + the_longest_palindrome(s[1:-1]) + s[-1]

        else:       
            a = the_longest_palindrome(s[1:])
            b = the_longest_palindrome(s[:-1])
            if len(a) > len(b):
                return a
            else:
                return b

print(the_longest_palindrome('1344631513'))


