import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon 

def sierpinski_triangle(n, points, color = 'orange'):
    if n == 0:
        return
    else:
        sierpinski_triangle(n-1, points/2, 'blue')
        sierpinski_triangle(n-1, points/2 + np.array([0.5, 0]), 'red')
        sierpinski_triangle(n-1, points/2 + np.array([0.25, 0.5]), 'green')
        plt.plot(points[:,0], points[:,1], color)
        print(points[:,0], points[:,1], color)
        print('points 1',points[:,0])
#  test equilateral triangle
        # plt.plot(-7.5, 2.5, marker = 'o')
        # plt.plot(-2.5, 2.5, marker = 'o')
        # plt.plot(-5.0, 7.5, marker = 'o')
        # plt.plot((-7.5, -2.5, -5.0, -7.5), (2.5, 2.5, 7.5, 2.5))

fig = plt.figure()  
fig.patch.set_facecolor('white')
ax = plt.gca()
triangle = np.array([[0,0],[0.5,1], [1,0],[0,0]])
# ax.add_patch(Polygon(triangle,
#                       fill = False))
sierpinski_triangle(5, triangle)                      
plt.axis('equal')
plt.grid(True, which='both')

# plt.axis('off')
plt.show()