import math
import numpy as np
import matplotlib.pyplot as plt

def rotate(angle):
    matrix = [[-1,-1],
             [-1,1],
             [1,1],
             [1,-1]]
    remainder = angle % 360
    if remainder == 0:
        return matrix
    elif remainder == 90:
         return [[-1,1],
                  [1,1],
                  [1,-1],
                  [-1,-1]]
    elif remainder == 180:
         return [[1,1],
                  [1,-1],
                  [-1,-1],
                  [-1,1]]
    elif remainder == 270:
         return [[1,-1],
                  [-1,-1],
                  [-1,1],
                  [1,1]]

def gilbert_show(n, center=(0, 0), size=1, angle=0):
    rm = rotate(angle) 
    firstPoint = (center[0] +rm[0][0]* size/2, center[1] + rm[0][1]*size/2)
    secondPoint = (center[0] + rm[1][0]*size/2, center[1] + rm[1][1]*size/2)
    thirdPoint = (center[0] + rm[2][0]*size/2, center[1] + rm[2][1]*size/2)
    fourthPoint = (center[0] + rm[3][0]*size/2, center[1] + rm[3][1]*size/2)      
    # plt.plot([firstPoint[0], secondPoint[0], thirdPoint[0], fourthPoint[0]])
    if n > 1:
      gilbert_show(n-1, firstPoint, size/2, angle+90)
      gilbert_show(n-1, secondPoint, size/2, angle+0)
      gilbert_show(n-1, thirdPoint, size/2, angle+0)
      gilbert_show(n-1, fourthPoint, size/2, angle-90)          
    if n == 1:  
      plt.plot([firstPoint[0], secondPoint[0]], [firstPoint[1], secondPoint[1]], 'k-')
      plt.plot([secondPoint[0], thirdPoint[0]], [secondPoint[1], thirdPoint[1]], 'k-')
      plt.plot([thirdPoint[0], fourthPoint[0]], [thirdPoint[1], fourthPoint[1]], 'k-')
    else:
        delimeter = pow(2, n)
        m = pow(2, n-1) - 1
        plt.plot([firstPoint[0] + m*rm[1][0]*size/delimeter, secondPoint[0]+m*rm[0][0]*size/delimeter], [firstPoint[1]+m*rm[1][1]*size/delimeter, secondPoint[1]+m*rm[0][1]*size/delimeter], 'k-')
        plt.plot([secondPoint[0] + m*rm[3][0]*size/delimeter, thirdPoint[0]+ m*rm[0][0]*size/delimeter], [secondPoint[1] + m*rm[3][1]*size/delimeter, thirdPoint[1]+ m*rm[0][1]*size/delimeter], 'k-')
        plt.plot([thirdPoint[0] + m*rm[3][0]*size/delimeter, fourthPoint[0]+m*rm[2][0]*size/delimeter], [thirdPoint[1]+m*rm[3][1]*size/delimeter, fourthPoint[1]+m*rm[2][1]*size/delimeter], 'k-')
     

   
            

        

    

fig = plt.figure()
fig.patch.set_facecolor('white')
gilbert_show(8)
plt.axis('equal')
plt.axis('on')
plt.show()


