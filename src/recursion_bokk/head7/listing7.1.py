import numpy as np
def exists_path_swap(A, r):
    if r < 0 or r >= A.shape[0] or A[r,0] == 'W':
        return False
    elif A.shape[1] == 1:
        return True
    else:
        return (
            exists_path_swap(A[:, 1:], r-1)
            or exists_path_swap(A[:, 1:], r)
            or exists_path_swap(A[:, 1:], r+1)
        )

A = np.array([[1,1,1],
              [1,'W',1],
              [1,1,'W']])

print(exists_path_swap(A, 0))
