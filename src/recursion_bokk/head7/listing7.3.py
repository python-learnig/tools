def towers_of_Hanoi(n, o, d, a):
    if n == 1:
        print("Move disk", n, "from rod", o, 'to rod', d),
    else:
        towers_of_Hanoi(n-1, o,a,d)
        print('Move disk', n, 'from rod', o, 'to rod', d),
        towers_of_Hanoi(n-1, a, d, o)


towers_of_Hanoi(4, 'O', 'D', 'A')
