def bit_count_recursion(n: int) -> int:
    if n == 1:
        return 1
    else:
        return count_zero_bit(n) + bit_count_recursion(n - 1)

def count_zero_bit(n: int) -> int:
    if n == 1 or n % 2 == 1:
        return 1
    else:    
        return 1 + count_zero_bit(n // 2)
print(bit_count_recursion(2)) #7 
