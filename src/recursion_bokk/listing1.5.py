def sum_list(list):
    if (len(list) == 0):
        return 0
    return (sum_list(list[0:len(list)-1]) + list[len(list)-1])

def sum_list_1(list):
    if(len(list) == 0):
        return 0
    return (list[0] + sum_list_1(list[1:len(list)-1]))

def sum_list_2(list):
    if(len(list)==0):
        return 0
    if(len(list)==1):
        return list[0]
    return sum_list_2(list[0:(len(list)-1)//2])+sum_list_2(list[(len(list)-1)//2:len(list)-1])
