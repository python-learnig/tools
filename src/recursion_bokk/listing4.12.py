def is_polindrom(s):
    n = len(s)
    if(n < 1):
        return True
    return s[1] == s[n-1]  and is_polindrom(s[1:n-1])

print(is_polindrom('aaaa'))
print(is_polindrom('abcd'))
