def linear_sort_recursion(s):
    if(len(s)== 1):
        return s[0]
    if(len(s) == 0):
        return []
    b = list(s)
    min_index = b.index(min(b))
    # get min element, change with first element
    indexMin = min(s)
    return indexMin + linear_sort_recursion(s[1:])
