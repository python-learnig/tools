def partion_hoare(a):
    left = 0
    right = len(a)-1
    middle = (left + right)//2
    pivot = a[middle]
    finished = False
    while not finished:
        while left < right and a[left] <= pivot:
            left = left +1

        while right > left and a[right] >pivot:
            right = right - 1
        if(left < right):
            aux = a[left]
            a[left] = a[right]
            a[right] = aux
        finished = left >= right

    return right

def partion_Hoare(a, lower, upper):
    if upper >=0:
        middle = (lower + upper) // 2
        pivot = a[middle]
        a[middle] = a[lower]
        a[lower] = pivot

        left = lower + 1
        right = upper
        finished = False
        while not finished:
            while left <= right and a[left] <= pivot:
                left = left +1
            while a[right] > pivot:
                right = right - 1
            if left < right:
                aux = a[left]
                a[left] = a[right]
                a[right] = aux
            finished = left >right

            a[lower] = a[right]
            a[right] = aux
        return right
            

a = [2,3,4,1,0,12,18,34,11,0,5,6,7,9]
print(partion_hoare(a))
print(a)

a = [2,3,4,1,0,12,18,34,11,0,5,6,7,9]

print(partion_Hoare(a,0, len(a)-1))
print(a)
