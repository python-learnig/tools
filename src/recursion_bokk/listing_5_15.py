def f(x):
    return x * (x-2)


def bisection(a,b,f, epsilon):
    z = (a-b)/2
    if f(z) == 0 or b -a <= 2*epsilon:
        return z
    elif (f(a) > 0 and f(z) < 0) or
