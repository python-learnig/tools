def gcdl(m,n):
    if m == 0:
        return n
    if m>n:
        return gcdl(n,m)
    else:
        return gcdl(m, n - m)
