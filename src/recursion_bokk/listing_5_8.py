def binary_search(a, x, lower, upper):
    if(lower > upper):
        return -1
    else:
        middle = (lower + upper) // 2

        if(x == a[middle]):
            return middle
        elif x < a[middle]:
            return binary_search(a, x, lower, middle-1)
        else:
            return binary_search(a, x, middle+1, upper)

def binary_search_wrapper(a, x):
    return binary_search(a, x, 0, len(a) -1)

a = [0,1,2,3,4,5,6,7,8,9]

print(binary_search_wrapper(a, 10))
