import sys
import unittest as test


def get_matrix(matrixData):
    matrix = [[float(i) for i in j.strip().split(' ')] for j in matrixData.splitlines()[1:]]
    return matrix


def firstIndexIsZero(matrix):
    return matrix[0][0] == 0


def firstRowIndexNotNull(matrix):
    for i in range(len(matrix)):
        if(matrix[i][0] != 0):
            return i
    return None


def changeMatrixFirstRow(matrix):
    firstRowIndex = firstRowIndexNotNull(matrix)
    changeRow(matrix, 0, firstRowIndex)
    return matrix

def changeRow(matrix, k, j):
    try:
        row_k = matrix[k]
        matrix[k] = matrix[j]
        matrix[j] = row_k
    except IndexError:
        pass


# modify matrix, that all columns
def modifyRow(rowToChange, zeroRow, multiply):
    rowToChange[0] = 0
    for i in range(1,len(zeroRow)):
        modifiedElement = rowToChange[i] - zeroRow[i]*multiply
        rowToChange[i] = modifiedElement
    return rowToChange


def modifyMatrix(matrix):
    firstElement = matrix[0][0]
    for i in range(1,len(matrix)):
        element = matrix[i][0]
        multiply = element/firstElement
        modifiedRow = modifyRow(matrix[i],matrix[0],multiply)
        matrix[i] = modifiedRow
    return matrix


def cropMatrix(modifiedMatrix):
    newMatrix = modifiedMatrix[1:]
    return [row[1:] for row in newMatrix]


def main_determinant(matrix):
    result = round(determinate(matrix))
    return result


def firstColumnIsZero(matrix):
    for i in range(len(matrix)):
        if(matrix[i][0] != 0):
            return False
    return True


def determinate(matrix):
    sign = 1
    if(len(matrix)==1):
        return matrix[0][0]
    if(firstColumnIsZero(matrix)):
        return 0
    if(firstIndexIsZero(matrix) == True):
        sign = sign *(-1)
        matrix = changeMatrixFirstRow(matrix)

    modifiedMatrix = modifyMatrix(matrix)
    multiplier = modifiedMatrix[0][0]
    newMatrix = cropMatrix(modifiedMatrix)
    return sign*multiplier*determinate(newMatrix)

if __name__ == '__main__':
    try:
        matrixData = sys.stdin.readlines()
        matrixData = ''.join(matrixData)
        matrix = get_matrix(matrixData)
        result = main_determinant(matrix)
        print(result)
    except(ValueError):
        print('Gone away')
        sys.exit(2)

class TestDeterminate(test.TestCase):
    def test_determinate(self):
        self.cases = (
                      {'matrix':[[1,2],[1,2]], 'expect':0},
                      {'matrix':[[4,2],[5,3]], 'expect':2},
                      {'matrix':[[2,1],[1,2]], 'expect':3},
                      {'matrix':[[5,7],[-4,1]], 'expect':33},
                      {'matrix':[[1.0]], 'expect':1},
                      {'matrix':[[5,7,1],[-4,1,0],[2,0,3]], 'expect':97},
                      {'matrix':[[1,0,-2],[3,2,1],[1,2,-2]], 'expect':-14},
                      {'matrix':[[0,5,-4],[3,2,1],[2,-4,5]], 'expect':-1},
                      {'matrix':[[2,4,2],[3,5,1],[0,1,2]], 'expect':0   },
                      {'matrix':[[2,1],[1,2]], 'expect':3},
                      {'matrix':[[2,1,0],[1,2,1],[0,1,2]], 'expect':4},
                      {'matrix':[[2,1,1,0],[1,2,1,1],[1,1,2,1],[0,1,1,2]], 'expect':4},
                      {'matrix':[[2,1,1,0],[1,2,1,1],[1,1,2,1],[0,1,1,2]], 'expect':4},
                      {'matrix': [[4,5,6,7], [4, 8, 6, -7], [4,0, 4, -1],[4,5,-3,7]], 'expect': -3384},

        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], main_determinant(x['matrix']))

    def test_firstIndexIsZero(self):
        self.cases = (
            {'matrix': [[1, 2], [1, 2]], 'expect':False},
            {'matrix': [[0, 2], [1, 2]], 'expect':True},
        )

        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], firstIndexIsZero(x['matrix']))
    def test_changeMatrixRow(self):
        self.cases = (
            {'matrix':[[1,2], [1,2]], 'expect':[[1,2], [1,2]]},
            {'matrix':[[0,2], [1,2]], 'expect':[[1,2], [0,2]]},
            {'matrix':[[1,2], [1,2],[0,2]], 'expect':[[1,2], [1,2],[0,2]]},
            {'matrix':[[0,2], [1,2],[0,2]], 'expect':[[1,2], [0,2],[0,2]]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], changeMatrixFirstRow(x['matrix']))

    def test_modifyMatrix(self):
        self.cases = (
            {'matrix':[[1,2], [0,2]], 'expect':[[2]]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], modifyMatrix(x['matrix']))

    def test_cropMatrix(self):
        self.cases = (
            {'matrix': [[1, 2], [2, 2]], 'expect': [[2]]},
            {'matrix': [[2, 2,3], [2, 2,3], [3, 2, 3]], 'expect': [ [2, 3], [2, 3]]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], cropMatrix(x['matrix']))

    def test_deteminate(self):
        self.cases = (
                      {'matrix':[[1,2],[1,2]], 'expect':0},
                      {'matrix':[[4,2],[5,3]], 'expect':2},
                      {'matrix':[[2,1],[1,2]], 'expect':3},
                      {'matrix':[[5,7],[-4,1]], 'expect':33},
                      {'matrix':[[1.0]], 'expect':1},
                      {'matrix':[[5,7,1],[-4,1,0],[2,0,3]], 'expect':97},
                      {'matrix':[[1,0,-2],[3,2,1],[1,2,-2]], 'expect':-14},
                      {'matrix':[[0.0, 2.0], [0.0, 0.0]], 'expect':0},
                      {'matrix': [[4,5,6,7], [4, 8, 6, -7], [4,0, 4, -1],[4,5,-3,7]], 'expect': -3384},

        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], main_determinant(x['matrix']))

