import unittest as test
import sys

from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.component = 0

    def addEdge(self, u, k):
        self.graph[u].append(k)
    def addPoint(self,u):
        self.graph[u-1]

    def bfs(self, start):
        visited, queue = set(), [start]
        queue.append(start)
        levels = 0
        levelsPath = {}
        levelsPath[0] = levels
        while queue:
            vertex = queue.pop(0)
            if vertex not in visited:
                visited.add(vertex)
                for child in self.graph[vertex]:
                    if(child not in levelsPath):
                        levelsPath[child] = levelsPath[vertex] + 1
                    if child not in visited:
                        queue.append(child)


        return levelsPath

    def sameLevels(self, siblings, path):
        for sibling in siblings:
            if sibling in path:
                return False
        return True



if __name__=='__main__':
    try:
         treeData = sys.stdin.readlines()
         treeData = [line.rstrip() for line in treeData]
         firstRow = treeData.pop(0)
         sizes = int(firstRow.split(' ')[0])
         graph = Graph()
         # for i in range(1,sizes+1):
         #     graph.addPoint(i)
         for row in treeData:
             graph.addEdge(int(row.split(' ')[0]), int(row.split(' ')[1]))
             graph.addEdge(int(row.split(' ')[1]), int(row.split(' ')[0]))
         path = graph.bfs(0)
         pathStr = ''
         for i in sorted(path.keys()):
             pathStr = pathStr + ' '+ str(path[i])

         print(pathStr.strip())
         # print(' '.join(str(path[e]) for e in path))

    except (ValueError):
        sys.exit(2)



