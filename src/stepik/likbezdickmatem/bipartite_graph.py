# dfs and mark green for i%2=1 circle and mark blue for i%2=0
# after that you should check another edges which should connected between only green or only blue points
import unittest as test
import sys
from copy import deepcopy

from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.component = 0
        self.path = []
        self.green = []
        self.blue = []
        self.color = []

    def addEdge(self, u, k):
            self.graph[u].append(k)
    def addPoint(self,u):
        self.graph[u]

    def DFSUtil(self, v, visited, color):
        visited[v] = True
        self.color[v] =color
        for i in self.copyGraph[v]:
                if self.color[i] == 0:
                    self.DFSUtil(i, visited, self.invertColor(color))
                else:
                    if self.color[i] == color:
                        raise Exception('Wrong color')


    def invertColor(self, color):
        if color == 1:
            return 2
        else:
            return 1

    def DFS(self):
        V = len(self.graph)
        visited = [False] * (V + 1)
        self.copyGraph = deepcopy(self.graph)
        self.color = [0] *(V+1)
        for i in range(1,V+1):
            if self.color[i] == 0:
                try:
                    self.DFSUtil(i, visited, 1)
                except Exception as e:
                    return 'NO'
        return 'YES'

if __name__=='__main__':
         treeData = sys.stdin.readlines()
         treeData = [line.rstrip() for line in treeData]
         firstRow = treeData.pop(0)
         sizes = int(firstRow.split(' ')[0])
         graph = Graph()
         for i in range(1,sizes+1):
             graph.addPoint(i)
         for row in treeData:
             graph.addEdge(int(row.split(' ')[0]), int(row.split(' ')[1]))
             graph.addEdge(int(row.split(' ')[1]), int(row.split(' ')[0]))
         else:
             result = graph.DFS()
             pathStr = ''
             if isinstance(result, list):
                 for i in result:
                     pathStr = pathStr + ' ' + str(i)
             else:
                 pathStr = result
         print(pathStr)
