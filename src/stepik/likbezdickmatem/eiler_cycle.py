import unittest as test
import sys
from copy import deepcopy

from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.component = 0
        self.path = []

    def addEdge(self, u, k):
            self.graph[u].append(k)
    def addPoint(self,u):
        self.graph[u]

    def DFSUtil(self, v, visited):
        visited[v] = True
        self.path.append(v)
        for i in self.copyGraph[v]:
            if(self.deadEnd(i) and self.existsAnotherSibllings(v)):
                continue
            self.removeEdge(v, i, self.copyGraph)
            self.DFSUtil(i, visited)

    def deadEnd(self, i):
        if(len(self.copyGraph[i]) <= 1):
            return True
        return False
    def existsAnotherSibllings(self, parent):
        if(len(self.copyGraph[parent]) > 1):
            return True
        return False
    def removeEdge(self, fromPoint, toPoint, graph):
        graph[fromPoint].remove(toPoint)
        graph[toPoint].remove(fromPoint)
    def graphIsEmpty(self, graph):
        V = len(graph)
        for i in range(1,V+1):
            if len(graph[i]) > 0:
                return False
        return True

    def sort(self, graph):
        V = len(graph)
        for i in range(1,V+1):
            graph[i].sort()

    def connected_components(self, graph):
        def visit(v):
            visited[v] = 1
            for w in graph[v]:
                if not visited[w]:
                    visit(w)

        visited = [0 for _ in range(0,len(graph)+1)]
        cc = 0

        for i in range(1,len(graph)+1):
            if not visited[i]:
                visit(i)
                cc += 1

        return cc

    def is_eulerian(self):
        if self.connected_components(self.graph) > 1:
            return False

        V = len(self.graph)
        for w in range(1,V+1):
            if len(self.graph[w]) % 2 != 0:
                return False

        return True

    def DFS(self):
        V = len(self.graph)
        for i in range(1,V+1):
            visited = [False] * (V + 1)
            if visited[i] == False:
                self.copyGraph = deepcopy(self.graph)
                self.sort(self.copyGraph)
                self.DFSUtil(i, visited)
                if(self.graphIsEmpty(self.copyGraph) and len(self.path)>= len(self.graph)):
                    #remove last element in the path
                    self.path.pop()
                    return self.path
                else:
                    self.path = []
        return 'NONE'



if __name__=='__main__':
         treeData = sys.stdin.readlines()
         treeData = [line.rstrip() for line in treeData]
         firstRow = treeData.pop(0)
         sizes = int(firstRow.split(' ')[0])
         graph = Graph()
         for i in range(1,sizes+1):
             graph.addPoint(i)
         for row in treeData:
             graph.addEdge(int(row.split(' ')[0]), int(row.split(' ')[1]))
             graph.addEdge(int(row.split(' ')[1]), int(row.split(' ')[0]))
         if (graph.is_eulerianconnected_components() == False):
             pathStr = 'NONE'
         else:
             result = graph.DFS()
             pathStr = ''
             if isinstance(result, list):
                 for i in result:
                     pathStr = pathStr + ' ' + str(i)
             else:
                 pathStr = result
         print(pathStr)



