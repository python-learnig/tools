import sys
import unittest as test
from math import gcd
import itertools

def func(k,n,shift,select,step):
    for i in range(shift,n-k+1+step):
        if (step==k-1):
            print(' '.join(str(e) for e in (select+ [i])))
        else:
            func(k,n,i+1,select+[i],step+1)




class TestFactorize(test.TestCase):

    def test_resolve_matrix(self):
        self.maxDiff = None
        self.cases = ({'n':1, 'k':1, 'expect':[]},
                      {'n':2, 'k':1, 'expect':[[0],[1]]},
                      {'n':3, 'k':2, 'expect':[[0,1],[0,2],[1,2]]},
                      {'n':2, 'k':4, 'expect':[[0, 1], [0, 2], [0, 3], [0, 4], [1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4]]},
                      {'n':4, 'k':3, 'expect':[[0, 1, 2], [0, 1, 3], [0, 2, 3], [1, 2, 3]]},
                      {'n':5, 'k':4, 'expect':[]},
                      {'n':5, 'k':3, 'expect':[]},
                      {'n':5, 'k':1, 'expect':[]},
                      {'n':7, 'k':5, 'expect':[]})
        self.cases = ({'n':4, 'k':2, 'expect':[]},)

        for x in self.cases:
            with self.subTest(case=x):
                print(func(x['k'], x['n'], 0, [],0))
