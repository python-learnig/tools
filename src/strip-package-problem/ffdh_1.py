import unittest as test

def sort_by_height(data):
    sorted_data = sorted(data.items(), key=lambda x: x[1]['h'], reverse=True)
    return sorted_data


def next_pallet(sortedPallets):
    return sortedPallets.pop(0)

def truck_settings():
    return {'Width':16}


def packTruck(track, pallet):
    track['width'] += pallet['w']


def fill_truck(pallets, truckSettigngs):
    trackLoad = {'width':0, 'height':0}
    h = [0 for i in range(10)]
    level = 0
    sortedPallets = sort_by_height(pallets)
    pallet = next_pallet(sortedPallets)
    packTruck(trackLoad,pallet[1])
    print(pallet, level)
    pallet[1]['loaded'] = True
    h[level] = pallet[1]['h']
    for pallet in sortedPallets:
        for palletCandidate in sortedPallets:
            if ('loaded' in palletCandidate[1]):
                continue
            if(truckSettigngs['Width'] - trackLoad['width'] >= palletCandidate[1]['w']):
                packTruck(trackLoad,palletCandidate[1])
                print(palletCandidate, level)
                palletCandidate[1]['loaded'] = True
        if('loaded' in pallet[1]):
            continue
        level = level+1
        trackLoad['width'] = pallet[1]['w']
        print(pallet,level)
        pallet[1]['loaded'] = True
        h[level] = h[level-1] + pallet[1]['h']
    return h[level]


class Test(test.TestCase):
    def testSort(self):
        self.cases = ({'data':{'L1': {'w': 14, 'h': 4},
        'L2': {'w': 2, 'h': 4},
        'L3': {'w': 9, 'h': 3},
        'L4': {'w': 10, 'h': 5},
        'L5': {'w': 4, 'h': 10},
        'L6': {'w': 4, 'h': 1},
        'L7': {'w': 2, 'h': 4},
        'L8': {'w': 8, 'h': 2},
        'L9': {'w': 3, 'h': 3},
        'L10': {'w': 5, 'h': 1},
        }, 'expected': [10, 5, 4, 4, 4, 3, 3, 2, 1, 1]},
      {'data': {'L1': {'w': 14, 'h': 4},
                'L2': {'w': 2, 'h': 1},
                'L3': {'w': 9, 'h': 5},
                'L4': {'w': 10, 'h': 5},
                'L5': {'w': 4, 'h': 14},
                'L6': {'w': 4, 'h': 1},
                'L7': {'w': 2, 'h': 4},
                'L8': {'w': 8, 'h': 2},
                'L9': {'w': 3, 'h': 3},
                'L10': {'w': 5, 'h': 1},
                }, 'expected': [14, 5, 5, 4, 4, 3, 2, 1, 1, 1]}
                      )

        for x in self.cases:
            with(self.subTest(case=x)):
                result = sort_by_height(x['data'])
                sortedHeight = []
                for pallet in result:
                    sortedHeight.append(pallet[1]['h'])
                self.assertEquals(sortedHeight, x['expected'])
    def testFillTrack(self):
        self.cases = ({'truckSettings':{'Width':16},'pallets':{
        'L1': {'w': 14, 'h': 4},
        'L2': {'w': 2, 'h': 4},
        'L3': {'w': 9, 'h': 3},
        'L4': {'w': 10, 'h': 5},
        'L5': {'w': 4, 'h': 10},
        'L6': {'w': 4, 'h': 1},
        'L7': {'w': 2, 'h': 6},
        'L8': {'w': 8, 'h': 2},
        'L9': {'w': 3, 'h': 3},
        'L10': {'w': 5, 'h': 1}},'expected':19},)
        for x in self.cases:
            self.assertEquals(fill_truck(x['pallets'], x['truckSettings']), x['expected'])
