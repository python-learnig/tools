dataSet = (
    {'Product code':'P1', 'Quantity':2, 'Width':41.0, 'Length':49.2, 'Height':62.0, 'Weight':824, 'Vertical split point':31.0},
    {'Product code':'P2', 'Quantity':1, 'Width':40.5, 'Length':49.9, 'Height':58.0, 'Weight':1000, 'Vertical split point':34.8},
    {'Product code':'P3', 'Quantity':2, 'Width':40.8, 'Length':48.6, 'Height':60.6, 'Weight':518, 'Vertical split point':30.3},
    {'Product code':'P4', 'Quantity':3, 'Width':41.0, 'Length':48.9, 'Height':58.0, 'Weight':540, 'Vertical split point':29.0},
    {'Product code':'P5', 'Quantity':5, 'Width':41.0, 'Length':48.9, 'Height':58.0, 'Weight':763, 'Vertical split point':29.0},
    {'Product code':'P6', 'Quantity':3, 'Width':41.0, 'Length':49.2, 'Height':62.0, 'Weight':520, 'Vertical split point':31.0},
    {'Product code':'P7', 'Quantity':5, 'Width':41.2, 'Length':51.6, 'Height':59.2, 'Weight':515, 'Vertical split point':29.6},
    {'Product code':'P8', 'Quantity':1, 'Width':40.8, 'Length':48.6, 'Height':60.6, 'Weight':1015, 'Vertical split point':30.3},
    {'Product code':'P9', 'Quantity':2, 'Width':40.8, 'Length':48.6, 'Height':57.9, 'Weight':80.2, 'Vertical split point':19.3},
    {'Product code':'P10', 'Quantity':2, 'Width':40.8, 'Length':48.6, 'Height':60.6, 'Weight':858, 'Vertical split point':30.3},
    {'Product code':'P11', 'Quantity':1, 'Width':40.5, 'Length':49.9, 'Height':58.0, 'Weight':864, 'Vertical split point':34.8},
    {'Product code':'P12', 'Quantity':2, 'Width':41.5, 'Length':49.5, 'Height':62.4, 'Weight':1029, 'Vertical split point':31.2},
    {'Product code':'P13', 'Quantity':1, 'Width':38.7, 'Length':50.7, 'Height':61.4, 'Weight':576, 'Vertical split point':30.7},
    {'Product code':'P14', 'Quantity':1, 'Width':42.3, 'Length':51.0, 'Height':55.8, 'Weight':603, 'Vertical split point':27.9},
    {'Product code':'P15', 'Quantity':1, 'Width':40.8, 'Length':48.6, 'Height':60.6, 'Weight':750, 'Vertical split point':30.3},
    {'Product code':'P16', 'Quantity':1, 'Width':40.8, 'Length':48.6, 'Height':60.6, 'Weight':896, 'Vertical split point':30.3},
           )

trailerMeasurent = {'length':504, 'width':94, 'height':100}

class Square:
    def __init__(self, products, trailerData):
        self.products = products
        self.trailerData = trailerData
    def getTrailerSquare(self):
        return self.trailerData['length']*self.trailerData['width']
    def getAUBA(self):
        square = 0
        quantity = 0
        for product in self.products:
         #    not approved formula
         unitQty = product['Height'] // product['Vertical split point']
         square += product['Width'] * product['Length'] * unitQty
         quantity += unitQty
        quantity = len(self.products)

        return square/quantity

    def NS(self):
        return self.getTrailerSquare()*0.95//self.getAUBA()
    def NEU(self):
        return 1.5*self.NS()



squareParams = Square(dataSet, trailerMeasurent)
print(squareParams.getTrailerSquare())
