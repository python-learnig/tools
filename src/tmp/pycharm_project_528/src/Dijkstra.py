def Dijkstra(N, S, matrix):
    valid = {i:True for i in N}
    weight = {i:1000000 for i in N}
    weight[S] = 0
    for i in N:
        min_weight = 1000001
        ID_min_weight = ''
        for i in weight:
            if valid[i] and weight[i] < min_weight:
                min_weight = weight[i]
                ID_min_weight = i
        for i in N:
            if i not in matrix[ID_min_weight]:
                matrix[ID_min_weight][i] = [0,1000000][i !=ID_min_weight]
            if weight[ID_min_weight] + matrix[ID_min_weight][i] < weight[i]:
                weight[i] = weight[ID_min_weight] + matrix[ID_min_weight][i]
        valid[ID_min_weight] = False
    return weight
matrix = {
    'B': {'A': 5, 'D': 1, 'G': 2},
    'A': {'B': 5, 'D': 3, 'E': 12, 'F' :5},
    'D': {'B': 1, 'G': 1, 'E': 1, 'A': 3},
    'G': {'B': 2, 'D': 1, 'C': 2},
    'C': {'G': 2, 'E': 1, 'F': 16},
    'E': {'A': 12, 'D': 1, 'C': 1, 'F': 2},
    'F': {'A': 5, 'E': 2, 'C': 16}}
nodes = ('A', 'B', 'C', 'D', 'E', 'F', 'G')
print(Dijkstra(nodes, 'B', matrix))