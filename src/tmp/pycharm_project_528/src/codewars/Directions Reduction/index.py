import unittest as test

def dirReduc(arr):
    directionsOpposites = {
                            'NORTH':'SOUTH',
                            'SOUTH':'NORTH',
                            'WEST':'EAST',
                            'EAST':'WEST'
    }
    processing = True
    i = 0
    while processing:
        isDirectionDelete = False
        if(len(arr) <= 1):
            return arr
        direction = arr[i]
        directionNested = directionsOpposites[direction]
        i +=1
        directionNext = arr[i]
        if (directionNested == directionNext and not isDirectionDelete):
                del arr[i]
                del arr[i-1]
                isDirectionDelete = True
                i =0
        if(i > len(arr)-2):
            processing = False
    return arr


# a = ["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]
# test.TestCase.assertEqual(['WEST'], ['WEST'])
# test.TestCase.assertEqual(dirReduc(a), ['WEST'])
# u=["NORTH", "WEST", "SOUTH", "EAST"]
# test.TestCase.assertEqual(dirReduc(u), ["NORTH", "WEST", "SOUTH", "EAST"])
class TestSolution(test.TestCase):
    def test_first(self):
        cases = ({'data':["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"], 'expect':['WEST']},
                 {'data':["NORTH", "WEST", "SOUTH", "EAST"], 'expect':["NORTH", "WEST", "SOUTH", "EAST"]},
                 {'data':['NORTH', 'NORTH', 'WEST', 'EAST'], 'expect':["NORTH", "NORTH"]},
                 )
        for b in cases:
            data = b['data']
            expected = b['expect']
            result = dirReduc(data)
            self.assertEqual(expected, result)
if __name__ == "__main__":
    test.main()