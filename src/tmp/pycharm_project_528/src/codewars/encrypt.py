import unittest as test
import math

"""
https://www.codewars.com/kata/simple-encryption-number-1-alternating-split
"""
def encrypt(world, n = 0 ):
    count = 0
    while count < n:
        count +=1
        oddSequence = [char for index,char in enumerate(world) if index%2 ==0 ]
        evenSequence = [char for index,char in enumerate(world) if (index+1)%2 ==0 ]
        world  = ''.join(evenSequence) + ''.join(oddSequence)
    return world

def decrypt(world, n = 0):
    count = 0
    decryptWorld = world
    while count < n:
        count +=1
        pivotLen = getEvenSequenceLen(world)
        evenSequence = world[0:pivotLen]
        oddSequence = world[pivotLen:]
        evenSequenceArray = list(evenSequence)
        oddSequenceArray = list(oddSequence)
        decryptWorld = ''
        while(len(oddSequenceArray)):
         decryptWorld = decryptWorld + oddSequenceArray.pop(0)
         if(len(evenSequenceArray)):
             decryptWorld = decryptWorld+ evenSequenceArray.pop(0)
        world = decryptWorld

    return decryptWorld

def getEvenSequenceLen(world):
    return math.floor(len(world)/2)

class TestEncrypt(test.TestCase):
    def test_encrypt(self):
        cases = (
            {'world':'This is a test!', 'count':1, 'expect':'hsi  etTi sats!'},
            {'world':'This is a test!', 'count':2, 'expect':'s eT ashi tist!'},
            {'world':'This is a test!', 'count':3, 'expect':' Tah itse sits!'},
            {'world':'This is a test!', 'count':4, 'expect':"This is a test!"},
            {'world':'This is a test!', 'count':-1, 'expect':'This is a test!'},
            {'world':"This kata is very interesting!", 'count':1, 'expect':"hskt svr neetn!Ti aai eyitrsig"}


        )
        for case in cases:
            with self.subTest(case):
                print(case)
                result = encrypt(case['world'], int(case['count']))
                self.assertEqual(case['expect'], result)

    def test_decrypt(self):
        cases = (
            {'world':'hsi  etTi sats!', 'count':1, 'expect':'This is a test!'},
            {'world':"This is a test!", 'count':0, 'expect':"This is a test!"},
            {'world':"s eT ashi tist!", 'count':2, 'expect':"This is a test!"},
            {'world': " Tah itse sits!", 'count': 3, 'expect': "This is a test!"},
            {'world': 'This is a test!', 'count': 4, 'expect': "This is a test!"},
            {'world': 'This is a test!', 'count': -1, 'expect': "This is a test!"},
            {'world': "hskt svr neetn!Ti aai eyitrsig", 'count': 1, 'expect': "This kata is very interesting!"}


        )
        for case in cases:
            with self.subTest(case):
                print(case)
                result = decrypt(case['world'], int(case['count']))
                self.assertEqual(case['expect'], result)
    def test_getEvenSequenceLen(self):
        cases = (
            {'world':'san', 'expect':1},
            {'world':'sana', 'expect':2},
            {'world':'sanan', 'expect':2},
            {'world':"hskt svr neetn!Ti aai eyitrsig", 'expect':15},
        )
        for case in cases:
            with self.subTest(case):
                print(case)
                result = getEvenSequenceLen(case['world'])
                self.assertEqual(case['expect'], result)


if __name__ == "__main__":
    test.main()