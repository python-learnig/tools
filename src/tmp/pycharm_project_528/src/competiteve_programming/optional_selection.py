def selection(price):
    total = [[0 for i in range(0,8)] for i in range(0, (1<<4))]
    for x in range(0,3):
        total[1<<x][0] = price[x][0]
    for d in range(1,7):
        for s in range(0,1<<3):
            total[s][d] = total[s][d-1]
            for x in range(0,3):
                if(s&(1<<x)):
                    total[s][d] = min(total[s][d], total[s^(1<<x)][d-1]+price[x][d])
    return total


price = [[6,9,5,2,8,9,1,6], [8,2,6,2,7,5,7,2],[5,3,9,7,3,5,1,4]]
print(selection(price))
