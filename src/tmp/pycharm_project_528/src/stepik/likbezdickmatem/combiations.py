import unittest as test
import sys
import itertools


def combinations(n,k):
    j=n
    combinations = []
    setN = getArray(n)
    sortSetN = reverse(setN[0:],0)
    combinations.append(setN[0:k])
    while j >= 0:
        if setN[j] > setN[j-1]:
            previousSort = setN[0:]
            setN = swap(setN, j-1)
            setN = sort(setN,j)
            if(setN[0:k] not in combinations):
               combinations.append((setN[0:k]))
            if(previousSort == setN):
                j=j-1
            else:
                j=n
        else:
            j = j -1
        if(setN == sortSetN):
            break

    return combinations

def swap(setN, index):
    value = setN[index]
    position = getBiggerValue(setN, index)
    setN[index] = setN[position]
    setN[position] = value
    return setN


def getBiggerValue(massive, index):
    value = massive[index]
    nextBiggerValue = sys.maxsize
    position = index
    for i, nextValue in enumerate(massive[index+1:len(massive)]):
        if(nextValue > value and nextValue < nextBiggerValue):
            position = i + index +1
            nextBiggerValue = nextValue
    return position

def reverse(set, index):
    set[set[index]:len(set)] = sorted(set[index:len(set)], reverse=True)
    return set
def sort(setN, index):
    setN[index:len(setN)] = sorted(setN[index:len(setN)])
    return setN

def getArray(n):
    lot =[]
    i = 0
    while i<=n:
        lot.append(i)
        i+=1
    return lot

def example(n,k):
    setN = getArray(n)
    print (list(itertools.permutations(setN)))

s=input().split()
n=int(s[1])
k=int(s[0])

class TestCombination(test.TestCase):


    def test_example(self):
        example(3,3)

    def test_swap(self):
        self.cases = ({'arr':[0,3,4,2,1], 'index':1, 'expect':[0,4,3,2,1]},
                      {'arr':[0,3,4,2,1], 'index':2, 'expect':[0,3,4,2,1]},)
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], swap(x['arr'], x['index']))
    def test_combinations(self):
        self.cases =({'n': 3, 'k': 2, 'expect': [[0], [1], [2]]},
                      {'n': 3, 'k': 1, 'expect': [[0], [1], [2]]},
                      {'n':2,'k':1, 'expect':[[0],[1],[2]]},
                      {'n':1,'k':2, 'expect':[[0,1],[1,0]]},)
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'],combinations(x['n'], x['k']))

    def test_reverse(self):
        self.cases = ({'arr':[0,1], 'index':0, 'expect':[1,0]},
                      {'arr':[0,1,2], 'index':1, 'expect':[0,2,1]},)
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], reverse(x['arr'], x['index']))
    def test_sort(self):
        self.cases = ({'arr':[1,0], 'index':0,'expect':[0,1]},
                      {'arr':[1,0,2,3], 'index':1,'expect':[1,0,2,3]},)
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], sort(x['arr'], x['index']))
