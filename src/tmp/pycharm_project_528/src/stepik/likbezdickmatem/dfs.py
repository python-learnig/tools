import unittest as test
from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.dfsSearch = list
        self.component = 0

    def addEdge(self, u, k):
        self.graph[u].append(k)

    def DFSUtil(self, v, visited):
        visited[v] = true
        self.dfsSearch.append(v)
        for i in self.graph[v]:
            if visited[i] == False:
                self.DFSUtil(i, visited)


    def DFS(self):
        V = len(self.graph)
        visited = [False]*(V)
        for i in range(V):
            if visited == False:
                self.component = self.component +1
                self.DFSUtil(i, visited)


class TestDFS(test.TestCase):
    def test_graph_dfs(self):
        graph = Graph()
        graph.addEdge(1,2)
        graph.addEdge(3,2)
        graph.DFS()
        self.assertEqual(2,graph.component)



