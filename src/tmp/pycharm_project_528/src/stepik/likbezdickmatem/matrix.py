import sys
import unittest as test
from math import gcd



precise = '.0000000000000001'
def get_matrix_size(matrixData):
    firstRow = matrixData.splitlines()[0]
    return {'rows':int(firstRow.split(' ')[1]), 'cols':int(firstRow.split(' ')[0])}
def validate_matrix_data(matrixSize, matrix):
    return matrixSize['rows'] == (len(matrix[0])- 1) and matrixSize['cols'] == len(matrix)

def get_matrix(matrixData):
    matrix = [[float(i) for i in j.strip().split(' ')] for j in matrixData.splitlines()[1:]]
    return matrix


def prepareRow(staticRow, variableRow, lcm, i):
    staticMultiplier = lcm / staticRow[i]
    if variableRow[i] == 0.0:
        return variableRow
    varibleMultiplier = lcm / variableRow[i]
    newStaticRow = [i*staticMultiplier for i in staticRow]
    newVariableRow = [i* varibleMultiplier for i in variableRow]
    return [newVariableRow[i] - newStaticRow[i] for i in range(len(newVariableRow))]


def determinate(matrix, k, i):
    if(len(matrix) <= k):
        return
    if(len(matrix[k]) <= i ):
        return
    i += 1
    changeRows = False
    j = 0
    while j < len(matrix):
        index = 0
        while index < (len(matrix[j])-1):
           if normalizeRow(matrix[j],index) and j!=index:
               changeRows = True
               changeRow(matrix, j, index)
           index += 1
        j +=1
    # while (i<len(matrix[k]) and k<(len(matrix)-1) and matrix[k][i] == 0 and matrix[k][k] == 0):
    #     changeRows = True
    #     row_k = matrix[k]
    #     matrix[k] = matrix[k+1]
    #     matrix[k+1] = row_k
    #     k+=1
    return changeRows


def matrixCouldSolve(matrix):
    result = True
    zeroArgument = zeroArguments(matrix)
    result = zeroArgument and result
    return result


def zeroArguments(matrix):

    for j in range(len(matrix)):
        zeroArguments = 0
        for i in range(len(matrix[j])-1):
            if matrix[j][i] == 0.0:
                zeroArguments +=1
        if zeroArguments == (len(matrix[j]) -1) and matrix[j][len(matrix[j])-1] != 0.0:
            return False
    return True


def checkToSolution(preparedRow, solutionRow):
    value = preparedRow[len(preparedRow)-1]
    solution = None
    index = 0
    for i in range(len(preparedRow)-1):

        if(abs(preparedRow[i]) > 0.000001):
            if solution == None:
                solution = value / preparedRow[i]
                index = i
            else:
                return True
    if solutionRow[index] != None and solutionRow[index] != solution:
        return False
    solutionRow[index] = solution
    return True


def create_lower_matrix(matrix):
    i= 0
    j=0
    solution = [None for i in range(len(matrix[j])-1)]
    while j < len(matrix):
        if(i >= (len(matrix[j])-1)):
            i=0
        value = matrix[j][i]
        k = 0
        if (checkToSolution(matrix[j], solution) == False):
            return 'NO'
        while(k < len(matrix)):
            if( k == i or normalizeRow(matrix[k],i)):
                k += 1
                continue
            k_value = matrix[k][i]
            if (k_value == 0):
                k += 1
                continue
            lcmValue = lcm(k_value, value)
            #if lcmValue = 0 change rows
            if abs(lcmValue) < 0.000001:
                changeRow(matrix, k, j)
                if(abs(value) < 0.000001):
                    # exists from cicle
                    k = len(matrix)
                    if(j < len(matrix)-1):
                        j -= 1
                    i -= 1
                k+=1
            else:
                preparedRow = prepareRow(matrix[j], matrix[k], lcmValue, i)
                matrix[k] = preparedRow
                if(determinate(matrix, k, i ) == False):
                    k+=1
                if(checkToSolution(preparedRow, solution) == False):
                    return 'NO'
            if(matrixCouldSolve(matrix) == False):
                return 'NO'
        i+=1
        j+=1
    return matrix

def changeRow(matrix, k, j):
      try:
        row_k = matrix[k]
        matrix[k] = matrix[j]
        matrix[j] = row_k
      except IndexError:
          pass


def normalizeRow(row, index):
    count = len(row)-1
    for i in range(len(row)-1):
        if(row[i] == 0.0):
            count -=1
    return count == 1 and (row[index] != 0.0)


def infResolving(lowerMatrix):
    j = 0
    while j < len(lowerMatrix):
        zeroRow = True
        for i in range(len(lowerMatrix[j])):
           zeroRow &= lowerMatrix[j][i] == 0.0
        if(zeroRow):
            lowerMatrix.pop(j)
            j -=1
        j+=1
    if len(lowerMatrix) == 0 or (len(lowerMatrix) < (len(lowerMatrix[0]) - 1)):
        return 'INF'


def resolve_args(lowerMatrix):
    args = []
    if(isinstance(lowerMatrix, str)):
        return lowerMatrix
    if(infResolving(lowerMatrix)):
        return 'INF'


    for j in range(len(lowerMatrix)):
        if j < len(lowerMatrix[j])-1:
            try:
                value = lowerMatrix[j][j]
                if value:
                  args.append(float(round(lowerMatrix[j][len(lowerMatrix[j]) -1 ]/ value,16)))
            except IndexError:
                continue
    return args


def convertToInt(a):
    while(a != int(a)):
        a = a*10
    return a
def lcm(a, b):
    a = convertToInt(a)
    b = convertToInt(b)
    return a * b // gcd(int(a), int(b))
#
# def lcm(a, b):
#     return (a,b)[a<b]


def resolve_matrix(matrixData):
    matrixSize = get_matrix_size(matrixData)
    matrix = get_matrix(matrixData)
    if(validate_matrix_data(matrixSize, matrix)):
       lowerMatrix = create_lower_matrix(matrix)
       args = resolve_args(lowerMatrix)
       if(isinstance(args, str)):
           return args
       # response = 'YES\n'
       # response += ' '.join(map(str,args))
       # return response
       return args





class TestFactorize(test.TestCase):
    def test_resolve_matrix(self):
        self.cases = ({'value':'''3 3
                                4 2 1 1
                                7 8 9 1
                                9 1 3 2''', 'expect':[0.2608695652173913,0.0434782608695652,-0.1304347826086956]},
                      {'value':'''2 3
                                  1 3 4 4
                                  2 1 4 5''', 'expect':'INF'},
                      {'value':'''3 3
                                  1 3 2 7
                                  2 6 4 8
                                  1 4 3 1''','expect':'NO'

                      },
                      {
                          'value':'''3 3
6 1 2 21 
4 -6 16 2 
3 8 1 2 ''',
                          'expect':[4.133333333333334, -1.1333333333333333, -1.3333333333333333]
                      },
                      {
                          'value':'''4 4
5 -3 2 -8 1 
1 1 1 1 0 
3 5 1 4 0 
4 2 3 1 3 ''',
                          'expect':[7, -8, -5, 6 ]
                      },
                      {'value':'''3 3
1 2 3 3 
3 5 7 0 
1 3 4 1 ''',
                       'expect':[-4, -13, 11]},
                      {
                          'value':'''4 4
2 5 4 1 20 
1 3 2 1 11 
2 10 9 7 40 
3 8 9 2 37''',
                          'expect':[1, 2, 2, 0]
                      },
                      {
                          'value':'''3 3
1 -2 1 0 
2 2 -1 3 
4 -1 1 5''',
                          'expect':[1, 2, 3]},
                      {
                          'value':'''4 4
3 2 1 1 -2 
1 -1 4 -1 -1 
-2 -2 -3 1 9 
1 5 -1 2 4''',
                          'expect':[-3, -1, 2, 7]
                      },
                      {
                          'value':'''3 3
0.12 0.18 -0.17 5.5 
0.06 0.09 0.15 -1.95 
0.22 -0.1 0.06 0.5''',
                          'expect':[10, 5, -20]
                      },
                      {
                          'value': '''3 3
                      1 3 2 7
                      2 6 4 8
                      1 4 3 1''',
                          'expect': 'NO'
                      },

                      {'value': '''4 3
2 -1 3 1 
2 -1 -1 -2 
4 -2 6 0 
6 8 -7 2 ''',
                          'expect': 'NO'
                      },
                      {'value': '''4 6
1 2 1 1 3 1 7 
1 2 1 2 1 -1 1 
1 2 1 -1 5 -1 2 
1 2 1 -2 -4 -4 -1''',
                          'expect': 'INF'
                      },
                      {'value': '''3 3
3 -2 1 0 
5 -14 15 0 
1 2 -3 0''',
                       'expect': 'INF'
                       },
                      {'value': '''5 4
2 3 -1 1 0 
2 7 -3 0 1 
0 4 -2 -1 1 
2 -1 1 2 -1 
4 10 -4 1 1''',
                       'expect': 'INF'
                       },
                      {'value': '''3 4
3 -5 2 4 2 
7 -4 1 3 5 
5 7 -4 -6 3''',
                       'expect': 'NO'
                       },
                      {'value': '''5 3
0 2 -1 -4 
1 -1 5 3 
2 1 -1 0 
3 2 3 -1 
3 4 2 -5 ''',
                       'expect': [1,-2,0]
                       },
                      {'value': '''4 3
1 1 -3 -1
2 1 -2 1
1 1 1 3
1 2 -3 1''',
                       'expect': 'NO'
                       },
                      {'value': '''4 1
        1 -1
        -2 2
        -3 3
        4 -4''',
                       'expect': [-1.0]
                       },
      {'value': '''4 1
      1 -1
      -2 0
      -3 3
      4 -4''',
       'expect': 'NO'
       },
  {'value': '''4 1
0 0
0 0
0 0
0 0''',
   'expect': 'INF'
   },
                      )
#         self.cases = (   {'value': '''4 1
# 0 0
# 0 0
# 0 0
# 0 0''',
#                        'expect': 'INF'
#                        },)
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], resolve_matrix(x['value']))

    def test_get_matrix_size(self):
        self.cases = ({'value':'''3 3
                                4 2 1 1
                                7 8 9 1
                                9 1 3 2''', 'expect':{'rows':3,'cols':3}},
                      {'value':'''3 2
                                  4 2 1 1
                                  7 8 9 1
                                  9 1 3 2''', 'expect':{'rows':2, 'cols':3}},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], get_matrix_size(x['value']))
    def test_validate_matrix_data(self):
        self.cases = (
                      {'matrixSize':{'rows':3, 'cols':3},'matrixData': [[4,2,1,1],
                                 [7,8,9,1],
                                 [9,1,3,2]], 'expect': True},
                      {'matrixSize': {'rows': 2, 'cols': 3}, 'matrixData': [[4, 2, 1, 1],
                                                                            [7, 8, 9, 1],
                                                                            [9, 1, 3, 2]], 'expect': False},
                      {'matrixSize': {'rows': 3, 'cols': 2}, 'matrixData': [[4, 2, 1, 1],
                                                                            [7, 8, 9, 1],
                                                                            [9, 1, 3, 2]], 'expect': False},
                      )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], validate_matrix_data(x['matrixSize'], x['matrixData']))

    def test_get_matrix(self):
        self.cases = (
                      {'value': '''3 3
                                   4 2 1 1
                                   7 8 9 1
                                   9 1 3 2''', 'expect': [[4,2,1,1],
                                                                                [7,8,9,1],
                                                                                [9,1,3,2]]},
                      )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], get_matrix(x['value']))

    def test_create_lower_matrix(self):
        self.cases = (
            {'value': [[4, 2, 1, 1],
                       [7, 8, 9, 1],
                       [9, 1, 3, 2]], 'expect': [[828.0, 0.0, 0.0, 216.0],
                                                [0.0, 4140.0, 0.0, 180.0],
                                                [0.0, 0.0, 230.0, -30.0]]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], create_lower_matrix(x['value']))
    def test_resolve_args(self):
        self.cases = (
            {'value': [[14, 0, 0, 14],
                       [0, 7, 0, 7],
                       [0, 0, 3, 3]], 'expect': [1,1,1]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], resolve_args(x['value']))
    def test_prepareRow(self):
        self.cases = (
            {'value': ([4, 2, 1, 1],
                       [7, 8, 9, 1],
                       28, 0 ), 'expect': [0.0, 18.0, 29.0, -3.0]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], prepareRow(staticRow=x['value'][0], variableRow=x['value'][1], lcm=x['value'][2], i=x['value'][3]))
    def test_lcm(self):


        self.cases = (
            {'value': (5,15),
                       'expect': 15},

            {'value': (15,15),
             'expect': 15.0},
            {'value': (3,7),
             'expect': 21.0},
            {'value': (3.0, 7),
             'expect': 21.0},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], lcm(a = x['value'][0], b = x['value'][1]))

    def test_convertToInt(self):


        self.cases = (
            {'value': 5,
             'expect': 5},
            {'value': 0.5,
             'expect': 5},
            {'value': 0.05,
             'expect': 5},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], convertToInt(x['value']))
    def test_matrixCouldSolve(self):
        self.cases = ( {'value': [[4.0, 0.0, 0.0, 3.0],
                                  [0.0, 1.0, 0.0, 2.0],
                                  [0.0, 0.0, 4.0, 4.0],
                                  [0.0, 0.0, 4.0, 5.0]], 'expect': False},)
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], matrixCouldSolve(x['value']))

    def tearDown(self):
        self.cases = None

counter = 0
if __name__=='__main__':
    try:
         # matrixData = ['3 3\n', '4 2 1 1\n', '7 8 9 1\n', '9 1 3 2\n']
         matrixData = sys.stdin.readlines()
         matrixData = ''.join(matrixData)
         if(counter > 3):
             print(matrixData)
         print(resolve_matrix(matrixData))
    except (ValueError):
        print('Gone away')
        sys.exit(2)