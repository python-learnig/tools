import sys
import unittest as test
from math import gcd

precise = '.0000000000000001'
def rotate_matrix(matrix):
    rotatedMatrix = [['n' for i in range(len(matrix))] for j in range(len(matrix[0]))]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            ir = j
            jt = i
            rotatedMatrix[ir][jt] = matrix[i][j]
    return rotatedMatrix

def scalar_multiply(v1, v2):
    scalar = sum([ v1[i]*v2[i] for i,j in enumerate(v2)])
    return scalar

# @todo тут ошибка, генерит лишний метод
def createMatrixForResolve(matrix):
    (vectors, f) = getFVector(matrix)
    matrixForResolve = [['n' for i in range(len(vectors)+1)] for j in range(len(vectors))]
    for j in range(len(vectors)):
        basisVector = vectors[j]
        for i in range(len(vectors)):
            index = i
            matrixForResolve[j][i] = scalar_multiply(basisVector, vectors[i])
        matrixForResolve[j][index+1] = scalar_multiply(f, vectors[j])
    return matrixForResolve

def resolve(matrixData):
    matrixSize = get_matrix_size(matrixData)
    matrix = get_matrix(matrixData)
    if (validate_matrix_data(matrixSize, matrix)):
        matrixForResolving = prepareMatrix(matrix)
        resolvedMatrix = resolve_matrix(matrixForResolving)
        return resolvedMatrix



def prepareMatrix(matrix):
    rotatedMatrix = rotate_matrix(matrix)
    matrixForResolving = createMatrixForResolve(rotatedMatrix)
    return matrixForResolving




def getFVector(matrix):
    f = matrix[len(matrix)-1]
    equationMatrix = matrix[:len(matrix)-1]
    return (equationMatrix, f)

def create_lower_matrix(matrix):
    i= 0
    j=0
    solution = [None for i in range(len(matrix[j])-1)]
    while j < len(matrix):
        if(i >= (len(matrix[j])-1)):
            i=0
        value = matrix[j][i]
        k = 0
        if (checkToSolution(matrix[j], solution) == False):
            return 'NO'
        while(k < len(matrix)):
            if( k == i or normalizeRow(matrix[k],i)):
                k += 1
                continue
            k_value = matrix[k][i]
            if (k_value == 0):
                k += 1
                continue
            lcmValue = lcm(k_value, value)
            #if lcmValue = 0 change rows
            if abs(lcmValue) < 0.000001:
                changeRow(matrix, k, j)
                if(abs(value) < 0.000001):
                    # exists from cicle
                    k = len(matrix)
                    if(j < len(matrix)-1):
                        j -= 1
                    i -= 1
                k+=1
            else:
                preparedRow = prepareRow(matrix[j], matrix[k], lcmValue, i)
                matrix[k] = preparedRow
                if(determinate(matrix, k, i ) == False):
                    k+=1
                if(checkToSolution(preparedRow, solution) == False):
                    return 'NO'
            if(matrixCouldSolve(matrix) == False):
                return 'NO'
        i+=1
        j+=1
    return matrix

def matrixCouldSolve(matrix):
    result = True
    zeroArgument = zeroArguments(matrix)
    result = zeroArgument and result
    return result

def zeroArguments(matrix):

    for j in range(len(matrix)):
        zeroArguments = 0
        for i in range(len(matrix[j])-1):
            if matrix[j][i] == 0.0:
                zeroArguments +=1
        if zeroArguments == (len(matrix[j]) -1) and matrix[j][len(matrix[j])-1] != 0.0:
            return False
    return True

def convertToInt(a):
    while(a != int(a)):
        a = a*10
    return a
def lcm(a, b):
    a = convertToInt(a)
    b = convertToInt(b)
    return a * b // gcd(int(a), int(b))

def determinate(matrix, k, i):
    if(len(matrix) <= k):
        return
    if(len(matrix[k]) <= i ):
        return
    i += 1
    changeRows = False
    j = 0
    while j < len(matrix):
        index = 0
        while index < (len(matrix[j])-1):
           if normalizeRow(matrix[j],index) and j!=index:
               changeRows = True
               changeRow(matrix, j, index)
           index += 1
        j +=1
    return changeRows

def prepareRow(staticRow, variableRow, lcm, i):
    staticMultiplier = lcm / staticRow[i]
    if variableRow[i] == 0.0:
        return variableRow
    varibleMultiplier = lcm / variableRow[i]
    newStaticRow = [i*staticMultiplier for i in staticRow]
    newVariableRow = [i* varibleMultiplier for i in variableRow]
    return [newVariableRow[i] - newStaticRow[i] for i in range(len(newVariableRow))]



def checkToSolution(preparedRow, solutionRow):
    value = preparedRow[len(preparedRow)-1]
    solution = None
    index = 0
    for i in range(len(preparedRow)-1):

        if(abs(preparedRow[i]) > 0.000001):
            if solution == None:
                solution = value / preparedRow[i]
                index = i
            else:
                return True
    if solutionRow[index] != None and solutionRow[index] != solution:
        return False
    solutionRow[index] = solution
    return True

def changeRow(matrix, k, j):
    try:
        row_k = matrix[k]
        matrix[k] = matrix[j]
        matrix[j] = row_k
    except IndexError:
        pass


def normalizeRow(row, index):
    count = len(row) - 1
    for i in range(len(row) - 1):
        if (row[i] == 0.0):
            count -= 1
    return count == 1 and (row[index] != 0.0)

def get_matrix_size(matrixData):
    firstRow = matrixData.splitlines()[0]
    return {'rows':int(firstRow.split(' ')[1]), 'cols':int(firstRow.split(' ')[0])}
def validate_matrix_data(matrixSize, matrix):
    return matrixSize['rows'] == (len(matrix[0])- 1) and matrixSize['cols'] == len(matrix)
def get_matrix(matrixData):
    matrix = [[float(i) for i in j.strip().split(' ')] for j in matrixData.splitlines()[1:]]
    return matrix
def resolve_matrix(matrix):

            lowerMatrix = create_lower_matrix(matrix)
            args = resolve_args(lowerMatrix)
            if(isinstance(args, str)):
                return args
            response=''
            response += ' '.join(map(str,args))
            return response


def resolve_args(lowerMatrix):
    args = []
    if(isinstance(lowerMatrix, str)):
        return lowerMatrix
    if(infResolving(lowerMatrix)):
        return 'INF'


    for j in range(len(lowerMatrix)):
        if j < len(lowerMatrix[j])-1:
            try:
                value = lowerMatrix[j][j]
                if value:
                  args.append(float(round(lowerMatrix[j][len(lowerMatrix[j]) -1 ]/ value,16)))
            except IndexError:
                continue
    return args

def infResolving(lowerMatrix):
    j = 0
    while j < len(lowerMatrix):
        zeroRow = True
        for i in range(len(lowerMatrix[j])):
           zeroRow &= lowerMatrix[j][i] == 0.0
        if(zeroRow):
            lowerMatrix.pop(j)
            j -=1
        j+=1
    if len(lowerMatrix) == 0 or (len(lowerMatrix) < (len(lowerMatrix[0]) - 1)):
        return 'INF'



if __name__=='__main__':
    try:
         # matrixData = ['3 3\n', '4 2 1 1\n', '7 8 9 1\n', '9 1 3 2\n']
         matrixData = sys.stdin.readlines()
         matrixData = ''.join(matrixData)
         print(resolve(matrixData))
    except (ValueError):
        print('Gone away')
        sys.exit(2)


class TestResolve(test.TestCase):
    def test_rotate_matrix(self):
        self.cases = (
            {'matrix':[[4,2,1,1],
                       [7,8,9,1],
                       [9,1,3,2]], 'expect':[[4,7,9],
                       [2,8,1],
                       [1,9,3],
                       [1,1,2]]},
            {'matrix': [[1, 1, 1],
                        [1, 0, 4],
                        [1, -1, 5]], 'expect': [[1,1,1],
                                                  [1,0,-1],
                                                  [1,4,5],
                                                ]},
            {'matrix': [[4, 2, 8],
                        [5, 2, 4],
                        [2, 6, 2],
                        [3, 0, 8]
                        ], 'expect': [[4, 5, 2, 3], [2, 2, 6, 0], [8, 4, 2, 8]]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], rotate_matrix(x['matrix']))
    def test_resolve(self):
        self.cases = (

            {'matrix': '''4 2
4 2 8 
5 2 4 
2 6 2 
3 0 8''', 'expect': [1.6531165311653115, -0.3089430894308943]},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], resolve(x['matrix']))

    def test_createMatrixForResolve(self):
        self.cases = (
            {'matrix':[[4,7,9],
                       [2,8,1],
                       [1,9,3],
                       ], 'expect':[[146, 73, 94], [73, 69, 77]]},
            {'matrix': [[4, 5, 2, 3],
                        [2, 2, 6, 0],
                        [8, 4, 2, 8]],
             'expect': [[54, 30, 80], [30, 44, 36]]}
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], createMatrixForResolve(x['matrix']))
    def test_prepareMatrix(self):
        self.cases = (
            {
              'matrix':[[1,-1,4],[1,0,5],[1,1,9]],
              'expect':[[3, 0, 18], [0, 2, 5]]
            },
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], prepareMatrix(x['matrix']))


    def test_getFVector(self):
        self.cases = (
            {'matrix':[[4,7,9],
                       [2,8,1],
                       [1,9,3],
                       [1,1,2]], 'expect':([[4,7,9],
                       [2,8,1],
                       [1,9,3]],[1,1,2])},
        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], getFVector(x['matrix']))


    def test_scalar_multiply(self):
        self.cases = ({'e1': [1, 0, 1], 'e2': [1, 0, 1], 'expect': 2},
                      {'e1': [1, 1, 1], 'e2': [1, 1, 1], 'expect': 3},
                      {'e1': [5, 2, 1], 'e2': [1, 1, 1], 'expect': 8}
                      )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], scalar_multiply(x['e1'], x['e2']))