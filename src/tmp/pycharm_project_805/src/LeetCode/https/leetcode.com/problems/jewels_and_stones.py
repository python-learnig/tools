# https://leetcode.com/problems/jewels-and-stones/
import unittest as test

class Solution:
    def numJewelsInStones(self, J: str, S: str) -> int:
        dict = {}
        for i in range(len(J)):
            if



class TestSolution(test.TestCase):
    def test_numJewelsInStones(self):
        self.cases = ({'J':'z', 'S':'ZZ', 'expect':0},{'J':'aA', 'S':'aAAbbbb', 'expect':3}
                      )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(Solution.numJewelsInStones(self, x['J'], x['S']), x['expect'])