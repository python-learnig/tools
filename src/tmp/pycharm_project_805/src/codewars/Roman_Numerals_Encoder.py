import unittest as test

"""
url of task
https://www.codewars.com/kata/roman-numerals-encoder/train/python
"""
romain = {1:'I',
              5:'V',
              10:'X',
              50:'L',
              100:'C',
              500:'D',
              1000:'M'}

def solution(n):

    return getRomainNumber(n, [])


def toRomainString(sequence):
    string = ''
    for s in sequence:
        if isinstance(s, tuple):
            for i in s:
                string += romain[i]
        else:
            string +=romain[s]
    return string

def getRomainNumber(number, sequence = []):
    sequence = []
    if (number in romain):
        return romain[number]
    numbers = decomposition(number)
    for ten in numbers:
        if ten == 0:
            continue
        if(ten in romain):
            sequence.append(ten)
            continue
        subCalculation = getSubCalculation(ten)
        if(subCalculation):
            sequence.append(subCalculation)
        else:
            sequence.append(getAddCalculation(ten))
    return toRomainString(sequence)


def getSubCalculation(number):
    nearestBigger = getNearestBigger(number)
    if  not nearestBigger:
        return
    diffNumber = nearestBigger - number
    if (diffNumber in romain):
        return (diffNumber, nearestBigger)

def getAddCalculation(number, sequence = ()):
    subCalculation = getSubCalculation(number)
    if not subCalculation:
        nearestSmaller = getNearestSmaller(number)
        sequence += (nearestSmaller,)
        diffNumber = number - nearestSmaller
        if(diffNumber in romain):
            sequence += (diffNumber,)
            return sequence

        return getAddCalculation(diffNumber, sequence)
    else:
        sequence += subCalculation
        return sequence


def getNearestSmaller(comparedNumber):
    for number, romainNumber in sorted(romain.items(), reverse=True):
        if(comparedNumber > number):
            return number


def getNearestBigger(checkedNumber):
    for number, romainNumber in romain.items():
        if checkedNumber < number:
            return number


def decomposition(number):
    tens = 1
    sequence = []
    if(number == 0):
        return [0]
    while(number > 0):
        ten = number % 10
        number = number // 10
        sequence.append(ten*tens)
        tens *= 10
    return sequence[::-1]

class TestSolution(test.TestCase):
    def test_first(self):
        cases = (
                 {'data': 91, 'expect':'XCI'},
                 {'data': 4, 'expect': 'IV'},
                 {'data': 6, 'expect': 'VI'},
                 {'data': 9, 'expect': 'IX'},
                 {'data': 90, 'expect': 'XC'},
                 {'data': 89, 'expect': 'LXXXIX'},
                 {'data': 91, 'expect': 'XCI'},
                 {'data': 984, 'expect': 'CMLXXXIV'},
                 {'data': 1000, 'expect': 'M'},
                 {'data': 1889, 'expect': 'MDCCCLXXXIX'},
                 {'data': 1989, 'expect': 'MCMLXXXIX'},
                 )
        for b in cases:
             with self.subTest(b):
                data = b['data']
                expected = b['expect']
                result = solution(data)
                self.assertEqual(expected, result)

    def test_getNearestBigger(self):
        self.cases = (
                {'number': 1001, 'expect':None},
                {'number': 90, 'expect':100},
                {'number': 4, 'expect':5},
                {'number': 2, 'expect':5},
                {'number':49, 'expect':50}
                )
        for b in self.cases:
            with self.subTest(case = b):
                self.assertEqual(b['expect'], getNearestBigger(b['number']))
    def test_getSubCalculation(self):
        self.cases = (
            {'number': 90, 'expect': (10, 100)},
            {'number': 9, 'expect': (1,10)},
            {'number': 4, 'expect': (1, 5)},
            {'number': 3, 'expect': None},
        )
        for b in self.cases:
            with self.subTest(case=b):
                self.assertEqual(b['expect'], getSubCalculation(b['number']))

    def test_getNearestSmaller(self):
        self.cases = (
            {'number': 90, 'expect': 50},
            {'number': 10, 'expect': 5},
            {'number': 5, 'expect': 1},
            {'number': 2, 'expect': 1},
        )
        for b in self.cases:
            with self.subTest(case=b):
                self.assertEqual(b['expect'], getNearestSmaller(b['number']))

    def test_getAddCalculation(self):
        self.cases = (
            {'number': 3, 'expect': (1,1,1)},
            {'number': 4, 'expect': (1, 5)},
            {'number':12, 'expect':(10,1,1)}
        )
        for b in self.cases:
            with self.subTest(case=b):
                self.assertEqual(b['expect'], getAddCalculation(b['number']))

    def test_toRomainString(self):
        self.cases = (
            {'number': [(1,5)], 'expect': 'IV'},
            {'number': [(10),(1,5)], 'expect': 'XIV'},
        )
        for b in self.cases:
            with self.subTest(case=b):
                self.assertEqual(b['expect'], toRomainString(b['number']))

    def test_decomposition(self):
        self.cases = (
            {'number': 10, 'expect': [10, 0]},
            {'number': 100, 'expect': [100, 0, 0]},
            {'number': 101, 'expect': [100, 0, 1]},
            {'number': 121, 'expect': [100, 20, 1]},
            {'number': 21, 'expect': [20, 1]},
        )
        for b in self.cases:
            with self.subTest(case=b):
                self.assertEqual(b['expect'], decomposition(b['number']))



if __name__=='__main__':
    test.main()

