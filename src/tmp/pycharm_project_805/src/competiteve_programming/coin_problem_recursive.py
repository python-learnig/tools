import math
coins = [1,2,3]
def solve(x):
    if x< 0:
        return math.inf
    if x == 0:
        return 0
    best = math.inf
    for i in coins:
        best = min(best, solve(x-i)+1)
    return best

print(solve(30))
