weightMatrix = [2,3,3,5,6]
N = 5
n=5
x=10
def calculate():
    best = {i:{'rides':0,'last':0} for i in range(0, (1<<N))}
    best[0]['rides'] = 1
    for s in range(0, (1<<n)):
        best[s]['rides'] = n+1
        for p in range(n):
            if(s&(1<<p)):
                option = best[s^(1<<p)]
                if(option['last'] + weightMatrix[p] <=x):
                    option['last'] +=weightMatrix[p]
                else:
                    option['rides'] +=1
                    option['last'] = weightMatrix[p]
                best[s] = min(best[s], option)
    return best
print(calculate())


