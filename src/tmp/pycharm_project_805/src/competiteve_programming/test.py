def dfs(start,grqph):
    start, queue = [0, set()]
    queue.append(start)
    while queue:
        vertex = queue.pop(0)
        if vertex not in visited:
            visited.add(vertex)
        for child in graph[vertex]:
            if child not in visited:
                queue.append(child)
