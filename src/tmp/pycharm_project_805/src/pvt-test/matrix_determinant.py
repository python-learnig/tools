import unittest as test

def determinate(matrix):
    detA = 0
    if(len(matrix) == 2):
        detA =  matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0]
    else:
        for k in range(len(matrix)):
            matrixK = [row[:k]+row[k+1:]  for i,row in enumerate(matrix) if i != 0]
            detA = detA + (-1)**(k)*matrix[0][k]*determinate(matrixK)
    return detA


class TestDeterminate(test.TestCase):
    def test_determinate(self):
        self.cases = (
                      {'matrix':[[1,2],[1,2]], 'expect':0},
                      {'matrix':[[2,1],[1,2]], 'expect':3},
                      {'matrix':[[5,7],[-4,1]], 'expect':33},
                      {'matrix':[[5,7,1],[-4,1,0],[2,0,3]], 'expect':97},
                      {'matrix':[[1,0,-2],[3,2,1],[1,2,-2]], 'expect':-14},
                      {'matrix': [[4,5,6,7], [4, 8, 6, -7], [4,0, 4, -1],[4,5,-3,7]], 'expect': -3384},

        )
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(x['expect'], determinate(x['matrix']))
