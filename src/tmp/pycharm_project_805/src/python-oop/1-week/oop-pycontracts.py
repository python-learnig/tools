from contracts import contract

@contract
def f(x:'int,>=0') -> 'int,>=0':
    return 0

f(1)