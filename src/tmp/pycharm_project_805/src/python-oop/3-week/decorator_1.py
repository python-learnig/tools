from  abc import ABC, abstractmethod
import unittest

class Hero:
    def __init__(self):
        self.positive_effects = []
        self.negative_effects = []

        self.stats = {
            "HP": 128,
            "MP": 42,
            "SP": 100,

            "Strength": 15,
            "Perception": 4,
            "Endurance": 8,
            "Charisma": 2,
            "Intelligence": 3,
            "Agility": 8,
            "Luck": 1
        }

    def get_positive_effects(self):
        return self.positive_effects.copy()

    def get_negative_effects(self):
        return self.negative_effects.copy()

    def get_stats(self):
        return self.stats.copy()

class AbstractEffect(Hero, ABC):

    def __init__(self, base):
        self.base = base

    @abstractmethod
    def get_stats(self):  # Возвращает итоговые хараетеристики
        pass

    @abstractmethod
    def get_positive_effects(self):
        pass

    @abstractmethod
    def get_negative_effects(self):
        pass

class AbstractNegotive(AbstractEffect):
    def __init__(self, base):
        AbstractEffect.__init__(self, base)
    def get_stats(self):
        parentStat = self.base.get_stats()
        negativeEffects = self.get_negative_effects()
        newStat = {}
        for key, value in parentStat.items():
            if (key in negativeEffects):
                value -= negativeEffects[key]
            newStat[key] = value
        return newStat

    def get_positive_effects(self):
        return []

    @abstractmethod
    def get_negative_effects(self):
        pass

class AbstractPositive(AbstractEffect):
    def __init__(self, base):
        AbstractEffect.__init__(self, base)

    def get_stats(self):
        parentStat = self.base.get_stats()
        positiveEffects = self.get_positive_effects()
        newStat = {}
        for key, value in parentStat.items():
            if(key in positiveEffects):
                value += positiveEffects[key]
            newStat[key] = value
        return newStat
    def get_negative_effects(self):
        return {}

    @abstractmethod
    def get_positive_effects(self):
        pass

class Berserk(AbstractPositive):

    def get_positive_effects(self):
        return {'Strength':7,
                'Endurance':7,
                'Agility':7,
                'Luck':7,
                'Perception':-3,
                'Charisma':-3,
                'Intelligence':-3,
                'HP':50
                }

class Weakness(AbstractNegotive):
    def get_negative_effects(self):
        return {'Strength':4, 'Agility':4, 'Endurance':4}


class TestDecorator(unittest.TestCase):

    def setUp(self):
        self.hero = Hero()
    def tearDown(self):
        self.hero = None

    def test_berserk(self):

        berserkEffects = Berserk(self.hero)
        self.assertEqual(22, berserkEffects.get_stats()['Strength'])
        self.assertEqual(15, berserkEffects.get_stats()['Endurance'])
        self.assertEqual(15, berserkEffects.get_stats()['Agility'])
        self.assertEqual(8, berserkEffects.get_stats()['Luck'])
        self.assertEqual(1, berserkEffects.get_stats()['Perception'])
        self.assertEqual(-1, berserkEffects.get_stats()['Charisma'])
        self.assertEqual(-1, berserkEffects.get_stats()['Charisma'])
        self.assertEqual(0, berserkEffects.get_stats()['Intelligence'])
        self.assertEqual(178, berserkEffects.get_stats()['HP'])
    # def test_first(self):
    #     self.assertEqual(1,1)
class TestWeakness(unittest.TestCase):
    def setUp(self):
        self.hero = Hero()

    def tearDown(self):
        self.hero = None

    def test_weakness(self):
        weakness = Weakness(self.hero)
        self.assertEqual(11, weakness.get_stats()['Strength'])
        self.assertEqual(4, weakness.get_stats()['Agility'])
        self.assertEqual(4, weakness.get_stats()['Endurance'])

class TestMultiply(unittest.TestCase):
    def setUp(self):
        self.hero = Hero()

    def tearDown(self):
        self.hero = None

    def test_multiply(self):
        bersek = Berserk(self.hero)
        multiply = Weakness(bersek)
        self.assertEqual(18, multiply.get_stats()['Strength'])

if __name__ == "__main__":
    unittest.main()