import unittest as test
import sys

from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.component = 0

    def addEdge(self, u, k):
        self.graph[u-1].append(k-1)
    def addPoint(self,u):
        self.graph[u-1]

    def DFSUtil(self, v, visited):
        visited[v] = True
        for i in self.graph[v]:
            if visited[i] == False:
                self.DFSUtil(i, visited)


    def DFS(self):
        V = len(self.graph)
        visited = [False]*(V)
        for i in range(V):
            if visited[i] == False:
                self.component = self.component +1
                self.DFSUtil(i, visited)


class TestDFS(test.TestCase):
    def test_graph_dfs(self):
        graph = Graph()
        graph.addPoint(1)
        graph.addPoint(2)
        graph.addPoint(3)
        graph.addPoint(4)
        graph.addEdge(1,2)
        graph.addEdge(2,1)
        graph.addEdge(3,2)
        graph.addEdge(2,3)
        graph.DFS()
        self.assertEqual(2,graph.component)
    def test_graph_dfs_2(self):
        graph = Graph()
        graph.addPoint(1)
        graph.addPoint(2)
        graph.addPoint(3)
        graph.addPoint(4)
        graph.addEdge(1, 2)
        graph.addEdge(2, 1)
        graph.addEdge(3, 2)
        graph.addEdge(2, 3)
        graph.addEdge(4, 3)
        graph.addEdge(3, 4)
        graph.DFS()
        self.assertEqual(1, graph.component)

if __name__=='__main__':
    try:
         treeData = sys.stdin.readlines()
         treeData = [line.rstrip() for line in treeData]
         firstRow = treeData.pop(0)
         sizes = int(firstRow.split(' ')[0])
         graph = Graph()
         for i in range(1,sizes+1):
             graph.addPoint(i)
         for row in treeData:
             graph.addEdge(int(row.split(' ')[0]), int(row.split(' ')[1]))
             graph.addEdge(int(row.split(' ')[1]), int(row.split(' ')[0]))
         graph.DFS()
         print(graph.component)

    except (ValueError):
        print(ValueError)
        sys.exit(2)



