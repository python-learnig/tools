# dfs and mark green for i%2=1 circle and mark blue for i%2=0
# after that you should check another edges which should connected between only green or only blue points
import unittest as test
import sys
from copy import deepcopy

from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.component = 0
        self.path = []
        self.green = []
        self.blue = []

    def addEdge(self, u, k):
            self.graph[u].append(k)
    def addPoint(self,u):
        self.graph[u]

    def DFSUtil(self, v, visited):
        visited[v] = True
        self.path.append(v)
        if(len(self.path) %2 == 1):
            if(v in self.blue):
                raise Exception('Vertex in another color')
            self.green.append(v)
        else:
            if (v in self.green):
                raise Exception('Vertex in another color')
            self.blue.append(v)
        for i in self.copyGraph[v]:
                self.removeEdge(v, i, self.copyGraph)
                self.DFSUtil(i, visited)
                self.path.pop()


    def removeEdge(self, fromPoint, toPoint, graph):
        graph[fromPoint].remove(toPoint)
        graph[toPoint].remove(fromPoint)
    def checkGraph(self, graph):
        V = len(graph)
        for i in range(1,V+1):
            parentVertex = i
            for childVertex in graph[parentVertex]:
                if(childVertex in self.blue and parentVertex in self.blue):
                     return False
                if(childVertex in self.green and parentVertex in self.green):
                    return False
        return True
    def notAllVertexVisited(self, visited):
        V = len(visited)+1
        for i in range(1,V):
            if (visited[i] == False):
                return True
        return False



    def DFS(self):
        V = len(self.graph)
        visited = [False] * (V + 1)
        for i in range(1,V+1):
            if visited[i] == False:
                self.copyGraph = deepcopy(self.graph)
                self.path = []
                try:
                    self.DFSUtil(i, visited)
                except Exception as e:
                    return 'NO'

        return 'YES'

if __name__=='__main__':
         treeData = sys.stdin.readlines()
         treeData = [line.rstrip() for line in treeData]
         firstRow = treeData.pop(0)
         sizes = int(firstRow.split(' ')[0])
         graph = Graph()
         for i in range(1,sizes+1):
             graph.addPoint(i)
         for row in treeData:
             graph.addEdge(int(row.split(' ')[0]), int(row.split(' ')[1]))
             graph.addEdge(int(row.split(' ')[1]), int(row.split(' ')[0]))
         else:
             result = graph.DFS()
             pathStr = ''
             if isinstance(result, list):
                 for i in result:
                     pathStr = pathStr + ' ' + str(i)
             else:
                 pathStr = result
         print(pathStr)
