def dfs(graph, start):
    queue = [start]
    visited = [False]*len(graph)
    vertex = queue.pop(0)
    if(visited[vertex] == false):
        visited[vertex] = true
    while queue:
        for i in graph[vertex]:
            if visited[i] == False:
                queue.append(i)



