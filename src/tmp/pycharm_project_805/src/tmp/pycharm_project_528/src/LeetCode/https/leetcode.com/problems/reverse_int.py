#https://leetcode.com/problems/reverse-integer/
import unittest as test
class Solution:
    def reverse(self, x: int) -> int:
        max_int = 2147483647
        sign =1
        if(x < 0):
            sign = -1
        result = int(str(abs(x))[::-1])
        if(result > max_int):
            return 0
        return sign * result




class TestSolution(test.TestCase):

    def test_twoSum(self):
        self.maxDiff = None
        self.cases = ({'integer':12, 'expect':21},
                      {'integer':120, 'expect':21},
                      {'integer':-120, 'expect':-21},
                      {'integer':1534236469, 'expect':0})
        for x in self.cases:
            with self.subTest(case=x):
                self.assertEqual(Solution.reverse(self, x['integer']), x['expect'])