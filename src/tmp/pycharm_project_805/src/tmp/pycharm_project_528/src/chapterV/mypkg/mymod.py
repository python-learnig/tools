def countLines(name):
    file = open(name)
    count = len(file.readlines())
    file.close()
    return count
def countChars(name):
    file = open(name)
    count = len(file.read())
    file.close()
    return count
def test(name):
    print(countLines(name))
    print(countChars(name))
if __name__ == "__main__":
    test('mymod.py')