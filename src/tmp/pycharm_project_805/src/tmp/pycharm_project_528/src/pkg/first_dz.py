#загружаем файл с текстом
# для работы будем использовать библиотеку NLTK (Natural Language Toolkit)
import nltk
import re
from nltk.stem import PorterStemmer
nltk.download()
fileName = 'howework.txt'

def getText(fileName):
    with open(fileName,'r') as fileObject:
        return  fileObject.read()


text = getText(fileName)
#после того как мы загрузили текст, с помощью библиотеки токенизируем по словам
def tokenize(text):
    #remove all not world symbols
    text = re.sub("[^А-Яа-я]", " ", text)
    text.lower()
    return [world.lower() for world in nltk.word_tokenize(text)]
tokens = tokenize(text)


# делаем стиминг
# класс для стиминга
class Porter:
    PERFECTIVEGROUND = re.compile(u"((ив|ивши|ившись|ыв|ывши|ывшись)|((?<=[ая])(в|вши|вшись)))$")
    REFLEXIVE = re.compile(u"(с[яь])$")
    ADJECTIVE = re.compile(u"(ее|ие|ые|ое|ими|ыми|ей|ий|ый|ой|ем|им|ым|ом|его|ого|ему|ому|их|ых|ую|юю|ая|яя|ою|ею)$")
    PARTICIPLE = re.compile(u"((ивш|ывш|ующ)|((?<=[ая])(ем|нн|вш|ющ|щ)))$")
    VERB = re.compile(
        u"((ила|ыла|ена|ейте|уйте|ите|или|ыли|ей|уй|ил|ыл|им|ым|ен|ило|ыло|ено|ят|ует|уют|ит|ыт|ены|ить|ыть|ишь|ую|ю)|((?<=[ая])(ла|на|ете|йте|ли|й|л|ем|н|ло|но|ет|ют|ны|ть|ешь|нно)))$")
    NOUN = re.compile(
        u"(а|ев|ов|ие|ье|е|иями|ями|ами|еи|ии|и|ией|ей|ой|ий|й|иям|ям|ием|ем|ам|ом|о|у|ах|иях|ях|ы|ь|ию|ью|ю|ия|ья|я)$")
    RVRE = re.compile(u"^(.*?[аеиоуыэюя])(.*)$")
    DERIVATIONAL = re.compile(u".*[^аеиоуыэюя]+[аеиоуыэюя].*ость?$")
    DER = re.compile(u"ость?$")
    SUPERLATIVE = re.compile(u"(ейше|ейш)$")
    I = re.compile(u"и$")
    P = re.compile(u"ь$")
    NN = re.compile(u"нн$")

    def stem(word):
        word = word.lower()
        word = word.replace(u'ё', u'е')
        m = re.match(Porter.RVRE, word)
        if m and m.groups():
            pre = m.group(1)
            rv = m.group(2)
            temp = Porter.PERFECTIVEGROUND.sub('', rv, 1)
            if temp == rv:
                rv = Porter.REFLEXIVE.sub('', rv, 1)
                temp = Porter.ADJECTIVE.sub('', rv, 1)
                if temp != rv:
                    rv = temp
                    rv = Porter.PARTICIPLE.sub('', rv, 1)
                else:
                    temp = Porter.VERB.sub('', rv, 1)
                    if temp == rv:
                        rv = Porter.NOUN.sub('', rv, 1)
                    else:
                        rv = temp
            else:
                rv = temp

            rv = Porter.I.sub('', rv, 1)

            if re.match(Porter.DERIVATIONAL, rv):
                rv = Porter.DER.sub('', rv, 1)

            temp = Porter.P.sub('', rv, 1)
            if temp == rv:
                rv = Porter.SUPERLATIVE.sub('', rv, 1)
                rv = Porter.NN.sub(u'н', rv, 1)
            else:
                rv = temp
            word = pre + rv
        return word

    stem = staticmethod(stem)


def getLemmer(tokens):
    stemmer = Porter()
    words = [{'original': word, 'stem': stemmer.stem(word)} for word in tokens if
             word not in nltk.corpus.stopwords.words("russian")]
    return words


words = getLemmer(tokens)


def isEqual(word, word1):
    return (word['stem'] == word1['stem']) or (nltk.edit_distance(word['stem'], word1['stem']) < 2) or (
                nltk.edit_distance(word['original'], word1['stem']) < 3)


# сейчас у нас получился словарь в котором слова являются ключом, а нормальный вид слова является значением
# теперь нам осталось только посчитать входящие слова, для предотвращения ошибок будем сравнивать слова и считать их
# одинаковыми, если расстояния Левенштейна у них не больше 1 для их "корня"
def getStatistics(words):
    statistics = {}
    firstWord = words.pop()
    statistics[firstWord['original']] = {'equals_words': [firstWord['original']], 'count': 1, 'stem': firstWord['stem']}
    for word in words:
        for wordKey in list(statistics):
            wordSet = statistics[wordKey]
            if (isEqual(word, wordSet)):
                if (word not in statistics[wordKey]['equals_words']):
                    statistics[wordKey]['equals_words'].append(word['original'])
                statistics[wordKey]['count'] += 1
            else:
                statistics[word['original']] = {'equals_words': [word['original']], 'count': 1, 'stem': word['stem']}
    pure_state = {word: statistics[word]['count'] for word in statistics}

    return pure_state


print(getStatistics(words))

