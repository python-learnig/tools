import random

m1 = [[random.random() for i in range(5)] for i in range(5)]
m2 = [[random.random() for i in range(5)] for i in range(5)]


def writeToFile(matrix, fileName):
    with open(fileName, "w") as file_object:
        file_object.writelines(' '.join(map(str, row)) + '\n' for row in matrix)


writeToFile(m1, 'm1.txt')
writeToFile(m2, 'm2.txt')


def readMatrixFromFile(fileName):
    m = []
    with open(fileName, 'r') as file_object:
        for line in file_object:
            m.append([i for i in map(float, line.strip().split())])
    return m


m1 = readMatrixFromFile('m1.txt')
m2 = readMatrixFromFile('m2.txt')


def myltiply_matrix(m1, m2):
    m = [[0 for i in range(len(m2))] for i in range(len(m1[0]))]
    for im,row in enumerate(m):
        for jm, row in enumerate(m[im]):
            for i, row in enumerate(m[im]):
                m[im][jm] = m[im][jm]+ m1[im][i]*m2[i][jm]
    return m


print(myltiply_matrix([[1,2],[3,4]], [[5,6],[7,8]]));


